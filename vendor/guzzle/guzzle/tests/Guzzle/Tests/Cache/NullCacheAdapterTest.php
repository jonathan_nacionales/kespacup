<?php

namespace Guzzle\Tests\Common\Cache;

use Guzzle\Cache\NullCacheAdapter;

/**
 * @covers Guzzle\Cache\NullCacheAdapter
 */
class NullCacheAdapterTest extends \Guzzle\Tests\GuzzleTestCase
{
    public function testNullCacheAdapter()
    {
        $c = new NullCacheAdapter();
        $this->assertEquals(false, $c->contains('bar'));
        $this->assertEquals(true, $c->delete('bar'));
        $this->assertEquals(false, $c->fetch('bar'));
        $this->assertEquals(true, $c->save('bar', 'bar'));
    }
}
