<?php

namespace Guzzle\Tests\Inflection;

use Guzzle\Inflection\PreComputedInflector;

/**
 * @covers Guzzle\Inflection\PreComputedInflector
 */
class PreComputedInflectorTest extends \Guzzle\Tests\GuzzleTestCase
{
    public function testUsesPreComputedHash()
    {
        $mock = $this->getMock('Guzzle\Inflection\Inflector', array('snake', 'camel'));
        $mock->expects($this->once())->method('snake')->with('Test')->will($this->returnValue('test'));
        $mock->expects($this->once())->method('camel')->with('Test')->will($this->returnValue('Test'));
        $inflector = new PreComputedInflector($mock, array('FooBar' => 'bar_bar'), array('bar_bar' => 'FooBar'));
        $this->assertEquals('FooBar', $inflector->camel('bar_bar'));
        $this->assertEquals('bar_bar', $inflector->snake('FooBar'));
        $this->assertEquals('Test', $inflector->camel('Test'));
        $this->assertEquals('test', $inflector->snake('Test'));
    }

    public function testMirrorsPrecomputedValues()
    {
        $mock = $this->getMock('Guzzle\Inflection\Inflector', array('snake', 'camel'));
        $mock->expects($this->never())->method('snake');
        $mock->expects($this->never())->method('camel');
        $inflector = new PreComputedInflector($mock, array('Zeep' => 'zeep'), array(), true);
        $this->assertEquals('Zeep', $inflector->camel('zeep'));
        $this->assertEquals('zeep', $inflector->snake('Zeep'));
    }

    public function testMirrorsPrecomputedValuesByMerging()
    {
        $mock = $this->getMock('Guzzle\Inflection\Inflector', array('snake', 'camel'));
        $mock->expects($this->never())->method('snake');
        $mock->expects($this->never())->method('camel');
        $inflector = new PreComputedInflector($mock, array('Zeep' => 'zeep'), array('bar' => 'Foo'), true);
        $this->assertEquals('Zeep', $inflector->camel('zeep'));
        $this->assertEquals('zeep', $inflector->snake('Zeep'));
        $this->assertEquals('Foo', $inflector->camel('bar'));
        $this->assertEquals('bar', $inflector->snake('Foo'));
    }
}
