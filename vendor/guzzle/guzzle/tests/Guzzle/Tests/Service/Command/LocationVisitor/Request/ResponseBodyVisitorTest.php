<?php

namespace Guzzle\Tests\Service\Command\LocationVisitor\Request;

use Guzzle\Service\Command\LocationVisitor\Request\ResponseBodyVisitor as Visitor;

/**
 * @covers Guzzle\Service\Command\LocationVisitor\Request\ResponseBodyVisitor
 */
class ResponseBodyVisitorTest extends AbstractVisitorTestCase
{
    public function testVisitsLocation()
    {
        $visitor = new Visitor();
        $param = $this->getNestedCommand('response_body')->getParam('bar');
        $visitor->visit($this->command, $this->request, $param, sys_get_temp_dir() . '/bar.txt');
        $body = $this->readAttribute($this->request, 'responseBody');
        $this->assertContains('/bar.txt', $body->getUri());
    }
}
