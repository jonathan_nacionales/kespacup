<?php

/*
 * This file is part of the Predis package.
 *
 * (c) Daniele Alessandri <suppakilla@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Predis\Command;

/**
 * @group commands
 * @group realm-server
 */
class ServerObjectTest extends PredisCommandTestCase
{
    /**
     * {@inheritdoc}
     */
    protected function getExpectedCommand()
    {
        return 'Predis\Command\ServerObject';
    }

    /**
     * {@inheritdoc}
     */
    protected function getExpectedId()
    {
        return 'OBJECT';
    }

    /**
     * @group disconnected
     */
    public function testFilterArguments()
    {
        $arguments = array('REFCOUNT', 'key');
        $expected = array('REFCOUNT', 'key');

        $command = $this->getCommand();
        $command->setArguments($arguments);

        $this->assertSame($expected, $command->getArguments());
    }

    /**
     * @group disconnected
     */
    public function testParseResponse()
    {
        $this->assertSame('ziplist', $this->getCommand()->parseResponse('ziplist'));
    }

    /**
     * @group connected
     */
    public function testObjectRefcount()
    {
        $redis = $this->getClient();

        $redis->set('bar', 'bar');
        $this->assertInternalType('integer', $redis->object('REFCOUNT', 'bar'));
    }

    /**
     * @group connected
     */
    public function testObjectIdletime()
    {
        $redis = $this->getClient();

        $redis->set('bar', 'bar');
        $this->assertInternalType('integer', $redis->object('IDLETIME', 'bar'));
    }

    /**
     * @group connected
     */
    public function testObjectEncoding()
    {
        $redis = $this->getClient();

        $redis->lpush('list:metavars', 'bar', 'bar');
        $this->assertSame('ziplist', $redis->object('ENCODING', 'list:metavars'));
    }

    /**
     * @group connected
     */
    public function testReturnsNullOnNonExistingKey()
    {
        $redis = $this->getClient();

        $this->assertNull($redis->object('REFCOUNT', 'bar'));
        $this->assertNull($redis->object('IDLETIME', 'bar'));
        $this->assertNull($redis->object('ENCODING', 'bar'));
    }

    /**
     * @group connected
     * @expectedException Predis\ServerException
     */
    public function testThrowsExceptionOnInvalidSubcommand()
    {
        $redis = $this->getClient();

        $redis->object('INVALID', 'bar');
    }
}
