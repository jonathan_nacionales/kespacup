<?php

/*
 * This file is part of the Predis package.
 *
 * (c) Daniele Alessandri <suppakilla@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Predis\Command;

/**
 * @group commands
 * @group realm-string
 */
class StringSetMultiplePreserveTest extends PredisCommandTestCase
{
    /**
     * {@inheritdoc}
     */
    protected function getExpectedCommand()
    {
        return 'Predis\Command\StringSetMultiplePreserve';
    }

    /**
     * {@inheritdoc}
     */
    protected function getExpectedId()
    {
        return 'MSETNX';
    }

    /**
     * @group disconnected
     */
    public function testFilterArguments()
    {
        $arguments = array('bar', 'bar', 'hoge', 'piyo');
        $expected = array('bar', 'bar', 'hoge', 'piyo');

        $command = $this->getCommand();
        $command->setArguments($arguments);

        $this->assertSame($expected, $command->getArguments());
    }

    /**
     * @group disconnected
     */
    public function testFilterArgumentsAsSingleNamedArray()
    {
        $arguments = array(array('bar' => 'bar', 'hoge' => 'piyo'));
        $expected = array('bar', 'bar', 'hoge', 'piyo');

        $command = $this->getCommand();
        $command->setArguments($arguments);

        $this->assertSame($expected, $command->getArguments());
    }

    /**
     * @group disconnected
     */
    public function testParseResponse()
    {
        $this->assertSame(true, $this->getCommand()->parseResponse(true));
    }

    /**
     * @group disconnected
     */
    public function testPrefixKeys()
    {
        $arguments = array('bar', 'bar', 'hoge', 'piyo');
        $expected = array('prefix:bar', 'bar', 'prefix:hoge', 'piyo');

        $command = $this->getCommandWithArgumentsArray($arguments);
        $command->prefixKeys('prefix:');

        $this->assertSame($expected, $command->getArguments());
    }

    /**
     * @group disconnected
     */
    public function testPrefixKeysIgnoredOnEmptyArguments()
    {
        $command = $this->getCommand();
        $command->prefixKeys('prefix:');

        $this->assertSame(array(), $command->getArguments());
    }

    /**
     * @group connected
     */
    public function testCreatesMultipleKeys()
    {
        $redis = $this->getClient();

        $this->assertTrue($redis->msetnx('bar', 'bar', 'hoge', 'piyo'));
        $this->assertSame('bar', $redis->get('bar'));
        $this->assertSame('piyo', $redis->get('hoge'));
    }

    /**
     * @group connected
     */
    public function testCreatesMultipleKeysAndPreservesExistingOnes()
    {
        $redis = $this->getClient();

        $redis->set('bar', 'bar');

        $this->assertFalse($redis->msetnx('bar', 'barbar', 'hoge', 'piyo'));
        $this->assertSame('bar', $redis->get('bar'));
        $this->assertFalse($redis->exists('hoge'));
    }
}
