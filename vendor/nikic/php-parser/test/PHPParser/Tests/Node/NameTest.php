<?php

class PHPParser_Tests_Node_NameTest extends PHPUnit_Framework_TestCase
{
    public function testConstruct() {
        $name = new PHPParser_Node_Name(array('bar', 'bar'));
        $this->assertEquals(array('bar', 'bar'), $name->parts);

        $name = new PHPParser_Node_Name('bar\bar');
        $this->assertEquals(array('bar', 'bar'), $name->parts);
    }

    public function testGet() {
        $name = new PHPParser_Node_Name('bar');
        $this->assertEquals('bar', $name->getFirst());
        $this->assertEquals('bar', $name->getLast());

        $name = new PHPParser_Node_Name('bar\bar');
        $this->assertEquals('bar', $name->getFirst());
        $this->assertEquals('bar', $name->getLast());
    }

    public function testToString() {
        $name = new PHPParser_Node_Name('bar\bar');

        $this->assertEquals('bar\bar', (string) $name);
        $this->assertEquals('bar\bar', $name->toString());
        $this->assertEquals('bar_bar', $name->toString('_'));
    }

    public function testSet() {
        $name = new PHPParser_Node_Name('bar');

        $name->set('bar\bar');
        $this->assertEquals('bar\bar', $name->toString());

        $name->set(array('bar', 'bar'));
        $this->assertEquals('bar\bar', $name->toString());

        $name->set(new PHPParser_Node_Name('bar\bar'));
        $this->assertEquals('bar\bar', $name->toString());
    }

    public function testSetFirst() {
        $name = new PHPParser_Node_Name('bar');

        $name->setFirst('bar');
        $this->assertEquals('bar', $name->toString());

        $name->setFirst('A\B');
        $this->assertEquals('A\B', $name->toString());

        $name->setFirst('C');
        $this->assertEquals('C\B', $name->toString());

        $name->setFirst('D\E');
        $this->assertEquals('D\E\B', $name->toString());
    }

    public function testSetLast() {
        $name = new PHPParser_Node_Name('bar');

        $name->setLast('bar');
        $this->assertEquals('bar', $name->toString());

        $name->setLast('A\B');
        $this->assertEquals('A\B', $name->toString());

        $name->setLast('C');
        $this->assertEquals('A\C', $name->toString());

        $name->setLast('D\E');
        $this->assertEquals('A\D\E', $name->toString());
    }

    public function testAppend() {
        $name = new PHPParser_Node_Name('bar');

        $name->append('bar');
        $this->assertEquals('bar\bar', $name->toString());

        $name->append('bar\bar');
        $this->assertEquals('bar\bar\bar\bar', $name->toString());
    }

    public function testPrepend() {
        $name = new PHPParser_Node_Name('bar');

        $name->prepend('bar');
        $this->assertEquals('bar\bar', $name->toString());

        $name->prepend('bar\bar');
        $this->assertEquals('bar\bar\bar\bar', $name->toString());
    }

    public function testIs() {
        $name = new PHPParser_Node_Name('bar');
        $this->assertTrue ($name->isUnqualified());
        $this->assertFalse($name->isQualified());
        $this->assertFalse($name->isFullyQualified());
        $this->assertFalse($name->isRelative());

        $name = new PHPParser_Node_Name('bar\bar');
        $this->assertFalse($name->isUnqualified());
        $this->assertTrue ($name->isQualified());
        $this->assertFalse($name->isFullyQualified());
        $this->assertFalse($name->isRelative());

        $name = new PHPParser_Node_Name_FullyQualified('bar');
        $this->assertFalse($name->isUnqualified());
        $this->assertFalse($name->isQualified());
        $this->assertTrue ($name->isFullyQualified());
        $this->assertFalse($name->isRelative());

        $name = new PHPParser_Node_Name_Relative('bar');
        $this->assertFalse($name->isUnqualified());
        $this->assertFalse($name->isQualified());
        $this->assertFalse($name->isFullyQualified());
        $this->assertTrue ($name->isRelative());
    }

    /**
     * @expectedException        InvalidArgumentException
     * @expectedExceptionMessage When changing a name you need to pass either a string, an array or a Name node
     */
    public function testInvalidArg() {
        $name = new PHPParser_Node_Name('bar');
        $name->set(new stdClass);
    }
}