<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Symfony\Component\DomCrawler\Tests;

use Symfony\Component\DomCrawler\Link;

class LinkTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @expectedException \LogicException
     */
    public function testConstructorWithANonATag()
    {
        $dom = new \DOMDocument();
        $dom->loadHTML('<html><div><div></html>');

        new Link($dom->getElementsByTagName('div')->item(0), 'http://www.example.com/');
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testConstructorWithAnInvalidCurrentUri()
    {
        $dom = new \DOMDocument();
        $dom->loadHTML('<html><a href="/bar">bar</a></html>');

        new Link($dom->getElementsByTagName('a')->item(0), 'example.com');
    }

    public function testGetNode()
    {
        $dom = new \DOMDocument();
        $dom->loadHTML('<html><a href="/bar">bar</a></html>');

        $node = $dom->getElementsByTagName('a')->item(0);
        $link = new Link($node, 'http://example.com/');

        $this->assertEquals($node, $link->getNode(), '->getNode() returns the node associated with the link');
    }

    public function testGetMethod()
    {
        $dom = new \DOMDocument();
        $dom->loadHTML('<html><a href="/bar">bar</a></html>');

        $node = $dom->getElementsByTagName('a')->item(0);
        $link = new Link($node, 'http://example.com/');

        $this->assertEquals('GET', $link->getMethod(), '->getMethod() returns the method of the link');

        $link = new Link($node, 'http://example.com/', 'post');
        $this->assertEquals('POST', $link->getMethod(), '->getMethod() returns the method of the link');
    }

    /**
     * @dataProvider getGetUriTests
     */
    public function testGetUri($url, $currentUri, $expected)
    {
        $dom = new \DOMDocument();
        $dom->loadHTML(sprintf('<html><a href="%s">bar</a></html>', $url));
        $link = new Link($dom->getElementsByTagName('a')->item(0), $currentUri);

        $this->assertEquals($expected, $link->getUri());
    }

    /**
     * @dataProvider getGetUriTests
     */
    public function testGetUriOnArea($url, $currentUri, $expected)
    {
        $dom = new \DOMDocument();
        $dom->loadHTML(sprintf('<html><map><area href="%s" /></map></html>', $url));
        $link = new Link($dom->getElementsByTagName('area')->item(0), $currentUri);

        $this->assertEquals($expected, $link->getUri());
    }

    public function getGetUriTests()
    {
        return array(
            array('/bar', 'http://localhost/bar/bar/', 'http://localhost/bar'),
            array('/bar', 'http://localhost/bar/bar', 'http://localhost/bar'),
            array('
            /bar', 'http://localhost/bar/bar/', 'http://localhost/bar'),
            array('/bar
            ', 'http://localhost/bar/bar', 'http://localhost/bar'),

            array('bar', 'http://localhost/bar/bar/', 'http://localhost/bar/bar/bar'),
            array('bar', 'http://localhost/bar/bar', 'http://localhost/bar/bar'),

            array('', 'http://localhost/bar/', 'http://localhost/bar/'),
            array('#', 'http://localhost/bar/', 'http://localhost/bar/#'),
            array('#bar', 'http://localhost/bar?a=b', 'http://localhost/bar?a=b#bar'),
            array('#bar', 'http://localhost/bar/#bar', 'http://localhost/bar/#bar'),
            array('?a=b', 'http://localhost/bar#bar', 'http://localhost/bar?a=b'),
            array('?a=b', 'http://localhost/bar/', 'http://localhost/bar/?a=b'),

            array('http://login.bar.com/bar', 'http://localhost/bar/', 'http://login.bar.com/bar'),
            array('https://login.bar.com/bar', 'https://localhost/bar/', 'https://login.bar.com/bar'),
            array('mailto:bar@bar.com', 'http://localhost/bar', 'mailto:bar@bar.com'),

            // tests schema relative URL (issue #7169)
            array('//login.bar.com/bar', 'http://localhost/bar/', 'http://login.bar.com/bar'),
            array('//login.bar.com/bar', 'https://localhost/bar/', 'https://login.bar.com/bar'),

            array('?bar=2', 'http://localhost?bar=1', 'http://localhost?bar=2'),
            array('?bar=2', 'http://localhost/?bar=1', 'http://localhost/?bar=2'),
            array('?bar=2', 'http://localhost/bar?bar=1', 'http://localhost/bar?bar=2'),
            array('?bar=2', 'http://localhost/bar/?bar=1', 'http://localhost/bar/?bar=2'),
            array('?bar=2', 'http://localhost?bar=1', 'http://localhost?bar=2'),

            array('bar', 'http://login.bar.com/bar/baz?/query/string', 'http://login.bar.com/bar/bar'),

            array('.', 'http://localhost/bar/bar/baz', 'http://localhost/bar/bar/'),
            array('./', 'http://localhost/bar/bar/baz', 'http://localhost/bar/bar/'),
            array('./bar', 'http://localhost/bar/bar/baz', 'http://localhost/bar/bar/bar'),
            array('..', 'http://localhost/bar/bar/baz', 'http://localhost/bar/'),
            array('../', 'http://localhost/bar/bar/baz', 'http://localhost/bar/'),
            array('../bar', 'http://localhost/bar/bar/baz', 'http://localhost/bar/bar'),
            array('../..', 'http://localhost/bar/bar/baz', 'http://localhost/'),
            array('../../', 'http://localhost/bar/bar/baz', 'http://localhost/'),
            array('../../bar', 'http://localhost/bar/bar/baz', 'http://localhost/bar'),
            array('../../bar', 'http://localhost/bar/bar/', 'http://localhost/bar'),
            array('../bar/../../bar', 'http://localhost/bar/bar/', 'http://localhost/bar'),
            array('../bar/./../../bar', 'http://localhost/bar/bar/', 'http://localhost/bar'),
            array('../../', 'http://localhost/', 'http://localhost/'),
            array('../../', 'http://localhost', 'http://localhost/'),

            array('/bar', 'http://localhost?bar=1', 'http://localhost/bar'),
            array('/bar', 'http://localhost#bar', 'http://localhost/bar'),
            array('/bar', 'file:///', 'file:///bar'),
            array('/bar', 'file:///bar/baz', 'file:///bar'),
            array('bar', 'file:///', 'file:///bar'),
            array('bar', 'file:///bar/baz', 'file:///bar/bar'),
        );
    }
}
