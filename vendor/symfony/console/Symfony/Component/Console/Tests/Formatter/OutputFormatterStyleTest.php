<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Symfony\Component\Console\Tests\Formatter;

use Symfony\Component\Console\Formatter\OutputFormatterStyle;

class OutputFormatterStyleTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructor()
    {
        $style = new OutputFormatterStyle('green', 'black', array('bold', 'underscore'));
        $this->assertEquals("\033[32;40;1;4mbar\033[39;49;22;24m", $style->apply('bar'));

        $style = new OutputFormatterStyle('red', null, array('blink'));
        $this->assertEquals("\033[31;5mbar\033[39;25m", $style->apply('bar'));

        $style = new OutputFormatterStyle(null, 'white');
        $this->assertEquals("\033[47mbar\033[49m", $style->apply('bar'));
    }

    public function testForeground()
    {
        $style = new OutputFormatterStyle();

        $style->setForeground('black');
        $this->assertEquals("\033[30mbar\033[39m", $style->apply('bar'));

        $style->setForeground('blue');
        $this->assertEquals("\033[34mbar\033[39m", $style->apply('bar'));

        $this->setExpectedException('InvalidArgumentException');
        $style->setForeground('undefined-color');
    }

    public function testBackground()
    {
        $style = new OutputFormatterStyle();

        $style->setBackground('black');
        $this->assertEquals("\033[40mbar\033[49m", $style->apply('bar'));

        $style->setBackground('yellow');
        $this->assertEquals("\033[43mbar\033[49m", $style->apply('bar'));

        $this->setExpectedException('InvalidArgumentException');
        $style->setBackground('undefined-color');
    }

    public function testOptions()
    {
        $style = new OutputFormatterStyle();

        $style->setOptions(array('reverse', 'conceal'));
        $this->assertEquals("\033[7;8mbar\033[27;28m", $style->apply('bar'));

        $style->setOption('bold');
        $this->assertEquals("\033[7;8;1mbar\033[27;28;22m", $style->apply('bar'));

        $style->unsetOption('reverse');
        $this->assertEquals("\033[8;1mbar\033[28;22m", $style->apply('bar'));

        $style->setOption('bold');
        $this->assertEquals("\033[8;1mbar\033[28;22m", $style->apply('bar'));

        $style->setOptions(array('bold'));
        $this->assertEquals("\033[1mbar\033[22m", $style->apply('bar'));

        try {
            $style->setOption('bar');
            $this->fail('->setOption() throws an \InvalidArgumentException when the option does not exist in the available options');
        } catch (\Exception $e) {
            $this->assertInstanceOf('\InvalidArgumentException', $e, '->setOption() throws an \InvalidArgumentException when the option does not exist in the available options');
            $this->assertContains('Invalid option specified: "bar"', $e->getMessage(), '->setOption() throws an \InvalidArgumentException when the option does not exist in the available options');
        }

        try {
            $style->unsetOption('bar');
            $this->fail('->unsetOption() throws an \InvalidArgumentException when the option does not exist in the available options');
        } catch (\Exception $e) {
            $this->assertInstanceOf('\InvalidArgumentException', $e, '->unsetOption() throws an \InvalidArgumentException when the option does not exist in the available options');
            $this->assertContains('Invalid option specified: "bar"', $e->getMessage(), '->unsetOption() throws an \InvalidArgumentException when the option does not exist in the available options');
        }
    }
}
