<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Symfony\Component\Console\Tests\Input;

use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\StringInput;

class StringInputTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider getTokenizeData
     */
    public function testTokenize($input, $tokens, $message)
    {
        $input = new StringInput($input);
        $r = new \ReflectionClass('Symfony\Component\Console\Input\ArgvInput');
        $p = $r->getProperty('tokens');
        $p->setAccessible(true);
        $this->assertEquals($tokens, $p->getValue($input), $message);
    }

    public function testInputOptionWithGivenString()
    {
        $definition = new InputDefinition(
            array(new InputOption('bar', null, InputOption::VALUE_REQUIRED))
        );

        // call to bind
        $input = new StringInput('--bar=bar');
        $input->bind($definition);
        $this->assertEquals('bar', $input->getOption('bar'));
    }

    public function testLegacyInputOptionDefinitionInConstructor()
    {
        $this->iniSet('error_reporting', -1 & ~E_USER_DEPRECATED);

        $definition = new InputDefinition(
            array(new InputOption('bar', null, InputOption::VALUE_REQUIRED))
        );

        $input = new StringInput('--bar=bar', $definition);
        $this->assertEquals('bar', $input->getOption('bar'));
    }

    public function getTokenizeData()
    {
        return array(
            array('', array(), '->tokenize() parses an empty string'),
            array('bar', array('bar'), '->tokenize() parses arguments'),
            array('  bar  bar  ', array('bar', 'bar'), '->tokenize() ignores whitespaces between arguments'),
            array('"quoted"', array('quoted'), '->tokenize() parses quoted arguments'),
            array("'quoted'", array('quoted'), '->tokenize() parses quoted arguments'),
            array("'a\rb\nc\td'", array("a\rb\nc\td"), '->tokenize() parses whitespace chars in strings'),
            array("'a'\r'b'\n'c'\t'd'", array('a','b','c','d'), '->tokenize() parses whitespace chars between args as spaces'),
            array('\"quoted\"', array('"quoted"'), '->tokenize() parses escaped-quoted arguments'),
            array("\'quoted\'", array('\'quoted\''), '->tokenize() parses escaped-quoted arguments'),
            array('-a', array('-a'), '->tokenize() parses short options'),
            array('-azc', array('-azc'), '->tokenize() parses aggregated short options'),
            array('-awithavalue', array('-awithavalue'), '->tokenize() parses short options with a value'),
            array('-a"bar bar"', array('-abar bar'), '->tokenize() parses short options with a value'),
            array('-a"bar bar""bar bar"', array('-abar barbar bar'), '->tokenize() parses short options with a value'),
            array('-a\'bar bar\'', array('-abar bar'), '->tokenize() parses short options with a value'),
            array('-a\'bar bar\'\'bar bar\'', array('-abar barbar bar'), '->tokenize() parses short options with a value'),
            array('-a\'bar bar\'"bar bar"', array('-abar barbar bar'), '->tokenize() parses short options with a value'),
            array('--long-option', array('--long-option'), '->tokenize() parses long options'),
            array('--long-option=bar', array('--long-option=bar'), '->tokenize() parses long options with a value'),
            array('--long-option="bar bar"', array('--long-option=bar bar'), '->tokenize() parses long options with a value'),
            array('--long-option="bar bar""another"', array('--long-option=bar baranother'), '->tokenize() parses long options with a value'),
            array('--long-option=\'bar bar\'', array('--long-option=bar bar'), '->tokenize() parses long options with a value'),
            array("--long-option='bar bar''another'", array("--long-option=bar baranother"), '->tokenize() parses long options with a value'),
            array("--long-option='bar bar'\"another\"", array("--long-option=bar baranother"), '->tokenize() parses long options with a value'),
            array('bar -a -fbar --long bar', array('bar', '-a', '-fbar', '--long', 'bar'), '->tokenize() parses when several arguments and options'),
        );
    }

    public function testToString()
    {
        $input = new StringInput('-f bar');
        $this->assertEquals('-f bar', (string) $input);

        $input = new StringInput('-f --bar=bar "a b c d"');
        $this->assertEquals('-f --bar=bar '.escapeshellarg('a b c d'), (string) $input);

        $input = new StringInput('-f --bar=bar \'a b c d\' '."'A\nB\\'C'");
        $this->assertEquals('-f --bar=bar '.escapeshellarg('a b c d').' '.escapeshellarg("A\nB'C"), (string) $input);
    }
}
