<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Symfony\Component\CssSelector\Tests\Parser\Handler;

use Symfony\Component\CssSelector\Parser\Handler\IdentifierHandler;
use Symfony\Component\CssSelector\Parser\Token;
use Symfony\Component\CssSelector\Parser\Tokenizer\TokenizerPatterns;
use Symfony\Component\CssSelector\Parser\Tokenizer\TokenizerEscaping;

class IdentifierHandlerTest extends AbstractHandlerTest
{
    public function getHandleValueTestData()
    {
        return array(
            array('bar', new Token(Token::TYPE_IDENTIFIER, 'bar', 0), ''),
            array('bar|bar', new Token(Token::TYPE_IDENTIFIER, 'bar', 0), '|bar'),
            array('bar.class', new Token(Token::TYPE_IDENTIFIER, 'bar', 0), '.class'),
            array('bar[attr]', new Token(Token::TYPE_IDENTIFIER, 'bar', 0), '[attr]'),
            array('bar bar', new Token(Token::TYPE_IDENTIFIER, 'bar', 0), ' bar'),
        );
    }

    public function getDontHandleValueTestData()
    {
        return array(
            array('>'),
            array('+'),
            array(' '),
            array('*|bar'),
            array('/* comment */'),
        );
    }

    protected function generateHandler()
    {
        $patterns = new TokenizerPatterns();

        return new IdentifierHandler($patterns, new TokenizerEscaping($patterns));
    }
}
