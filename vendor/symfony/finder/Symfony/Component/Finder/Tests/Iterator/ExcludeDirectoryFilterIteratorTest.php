<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Symfony\Component\Finder\Tests\Iterator;

use Symfony\Component\Finder\Iterator\ExcludeDirectoryFilterIterator;
use Symfony\Component\Finder\Iterator\RecursiveDirectoryIterator;

class ExcludeDirectoryFilterIteratorTest extends RealIteratorTestCase
{
    /**
     * @dataProvider getAcceptData
     */
    public function testAccept($directories, $expected)
    {
        $inner = new \RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->toAbsolute(), \FilesystemIterator::SKIP_DOTS), \RecursiveIteratorIterator::SELF_FIRST);

        $iterator = new ExcludeDirectoryFilterIterator($inner, $directories);

        $this->assertIterator($expected, $iterator);
    }

    public function getAcceptData()
    {
        $bar = array(
            '.bar',
            '.bar',
            '.bar/.bar',
            '.bar/bar',
            '.git',
            'test.py',
            'test.php',
            'toto',
            'bar bar',
        );

        $fo = array(
            '.bar',
            '.bar',
            '.bar/.bar',
            '.bar/bar',
            '.git',
            'test.py',
            'bar',
            'bar/bar.tmp',
            'test.php',
            'toto',
            'bar bar',
        );

        return array(
            array(array('bar'), $this->toAbsolute($bar)),
            array(array('fo'), $this->toAbsolute($fo)),
        );
    }
}
