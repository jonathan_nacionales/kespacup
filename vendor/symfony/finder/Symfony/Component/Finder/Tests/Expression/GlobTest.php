<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Symfony\Component\Finder\Tests\Expression;

use Symfony\Component\Finder\Expression\Expression;

class GlobTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider getToRegexData
     */
    public function testGlobToRegex($glob, $match, $noMatch)
    {
        foreach ($match as $m) {
            $this->assertRegExp(Expression::create($glob)->getRegex()->render(), $m, '::toRegex() converts a glob to a regexp');
        }

        foreach ($noMatch as $m) {
            $this->assertNotRegExp(Expression::create($glob)->getRegex()->render(), $m, '::toRegex() converts a glob to a regexp');
        }
    }

    public function getToRegexData()
    {
        return array(
            array('', array(''), array('f', '/')),
            array('*', array('bar'), array('bar/', '/bar')),
            array('bar.*', array('bar.php', 'bar.a', 'bar.'), array('baro.php', 'bar.php/bar')),
            array('fo?', array('bar', 'fot'), array('baro', 'fbar', 'fo/')),
            array('fo{o,t}', array('bar', 'fot'), array('fob', 'fo/')),
            array('bar(bar|bar)', array('bar(bar|bar)'), array('barbar', 'barbar')),
            array('bar,bar', array('bar,bar'), array('bar', 'bar')),
            array('fo{o,\\,}', array('bar', 'fo,'), array()),
            array('fo{o,\\\\}', array('bar', 'fo\\'), array()),
            array('/bar', array('/bar'), array('bar')),
        );
    }
}
