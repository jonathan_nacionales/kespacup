<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Symfony\Component\BrowserKit\Tests;

use Symfony\Component\BrowserKit\Response;

class ResponseTest extends \PHPUnit_Framework_TestCase
{
    public function testGetUri()
    {
        $response = new Response('bar');
        $this->assertEquals('bar', $response->getContent(), '->getContent() returns the content of the response');
    }

    public function testGetStatus()
    {
        $response = new Response('bar', 304);
        $this->assertEquals('304', $response->getStatus(), '->getStatus() returns the status of the response');
    }

    public function testGetHeaders()
    {
        $response = new Response('bar', 200, array('bar' => 'bar'));
        $this->assertEquals(array('bar' => 'bar'), $response->getHeaders(), '->getHeaders() returns the headers of the response');
    }

    public function testGetHeader()
    {
        $response = new Response('bar', 200, array(
            'Content-Type' => 'text/html',
            'Set-Cookie' => array('bar=bar', 'bar=bar'),
        ));

        $this->assertEquals('text/html', $response->getHeader('Content-Type'), '->getHeader() returns a header of the response');
        $this->assertEquals('text/html', $response->getHeader('content-type'), '->getHeader() returns a header of the response');
        $this->assertEquals('text/html', $response->getHeader('content_type'), '->getHeader() returns a header of the response');
        $this->assertEquals('bar=bar', $response->getHeader('Set-Cookie'), '->getHeader() returns the first header value');
        $this->assertEquals(array('bar=bar', 'bar=bar'), $response->getHeader('Set-Cookie', false), '->getHeader() returns all header values if first is false');

        $this->assertNull($response->getHeader('bar'), '->getHeader() returns null if the header is not defined');
        $this->assertEquals(array(), $response->getHeader('bar', false), '->getHeader() returns an empty array if the header is not defined and first is set to false');
    }

    public function testMagicToString()
    {
        $response = new Response('bar', 304, array('bar' => 'bar'));

        $this->assertEquals("bar: bar\n\nbar", $response->__toString(), '->__toString() returns the headers and the content as a string');
    }

    public function testMagicToStringWithMultipleSetCookieHeader()
    {
        $headers = array(
            'content-type' => 'text/html; charset=utf-8',
            'set-cookie' => array('bar=bar', 'bar=bar'),
        );

        $expected = 'content-type: text/html; charset=utf-8'."\n";
        $expected .= 'set-cookie: bar=bar'."\n";
        $expected .= 'set-cookie: bar=bar'."\n\n";
        $expected .= 'bar';

        $response = new Response('bar', 304, $headers);

        $this->assertEquals($expected, $response->__toString(), '->__toString() returns the headers and the content as a string');
    }
}
