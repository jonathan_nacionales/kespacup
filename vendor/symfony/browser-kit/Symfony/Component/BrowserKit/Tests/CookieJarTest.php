<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Symfony\Component\BrowserKit\Tests;

use Symfony\Component\BrowserKit\CookieJar;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\BrowserKit\Response;

class CookieJarTest extends \PHPUnit_Framework_TestCase
{
    public function testSetGet()
    {
        $cookieJar = new CookieJar();
        $cookieJar->set($cookie = new Cookie('bar', 'bar'));

        $this->assertEquals($cookie, $cookieJar->get('bar'), '->set() sets a cookie');

        $this->assertNull($cookieJar->get('barbar'), '->get() returns null if the cookie does not exist');

        $cookieJar->set($cookie = new Cookie('bar', 'bar', time() - 86400));
        $this->assertNull($cookieJar->get('bar'), '->get() returns null if the cookie is expired');
    }

    public function testExpire()
    {
        $cookieJar = new CookieJar();
        $cookieJar->set($cookie = new Cookie('bar', 'bar'));
        $cookieJar->expire('bar');
        $this->assertNull($cookieJar->get('bar'), '->get() returns null if the cookie is expired');
    }

    public function testAll()
    {
        $cookieJar = new CookieJar();
        $cookieJar->set($cookie1 = new Cookie('bar', 'bar'));
        $cookieJar->set($cookie2 = new Cookie('bar', 'bar'));

        $this->assertEquals(array($cookie1, $cookie2), $cookieJar->all(), '->all() returns all cookies in the jar');
    }

    public function testClear()
    {
        $cookieJar = new CookieJar();
        $cookieJar->set($cookie1 = new Cookie('bar', 'bar'));
        $cookieJar->set($cookie2 = new Cookie('bar', 'bar'));

        $cookieJar->clear();

        $this->assertEquals(array(), $cookieJar->all(), '->clear() expires all cookies');
    }

    public function testUpdateFromResponse()
    {
        $response = new Response('', 200, array('Set-Cookie' => 'bar=bar'));

        $cookieJar = new CookieJar();
        $cookieJar->updateFromResponse($response);

        $this->assertEquals('bar', $cookieJar->get('bar')->getValue(), '->updateFromResponse() updates cookies from a Response objects');
    }

    public function testUpdateFromSetCookie()
    {
        $setCookies = array('bar=bar');

        $cookieJar = new CookieJar();
        $cookieJar->set(new Cookie('bar', 'bar'));
        $cookieJar->updateFromSetCookie($setCookies);

        $this->assertInstanceOf('Symfony\Component\BrowserKit\Cookie', $cookieJar->get('bar'));
        $this->assertInstanceOf('Symfony\Component\BrowserKit\Cookie', $cookieJar->get('bar'));
        $this->assertEquals('bar', $cookieJar->get('bar')->getValue(), '->updateFromSetCookie() updates cookies from a Set-Cookie header');
        $this->assertEquals('bar', $cookieJar->get('bar')->getValue(), '->updateFromSetCookie() keeps existing cookies');
    }

    public function testUpdateFromEmptySetCookie()
    {
        $cookieJar = new CookieJar();
        $cookieJar->updateFromSetCookie(array(''));
        $this->assertEquals(array(), $cookieJar->all());
    }

    public function testUpdateFromSetCookieWithMultipleCookies()
    {
        $timestamp = time() + 3600;
        $date = gmdate('D, d M Y H:i:s \G\M\T', $timestamp);
        $setCookies = array(sprintf('bar=bar; expires=%s; domain=.symfony.com; path=/, bar=bar; domain=.blog.symfony.com, PHPSESSID=id; expires=%s', $date, $date));

        $cookieJar = new CookieJar();
        $cookieJar->updateFromSetCookie($setCookies);

        $barCookie = $cookieJar->get('bar', '/', '.symfony.com');
        $barCookie = $cookieJar->get('bar', '/', '.blog.symfony.com');
        $phpCookie = $cookieJar->get('PHPSESSID');

        $this->assertInstanceOf('Symfony\Component\BrowserKit\Cookie', $barCookie);
        $this->assertInstanceOf('Symfony\Component\BrowserKit\Cookie', $barCookie);
        $this->assertInstanceOf('Symfony\Component\BrowserKit\Cookie', $phpCookie);
        $this->assertEquals('bar', $barCookie->getValue());
        $this->assertEquals('bar', $barCookie->getValue());
        $this->assertEquals('id', $phpCookie->getValue());
        $this->assertEquals($timestamp, $barCookie->getExpiresTime());
        $this->assertNull($barCookie->getExpiresTime());
        $this->assertEquals($timestamp, $phpCookie->getExpiresTime());
    }

    /**
     * @dataProvider provideAllValuesValues
     */
    public function testAllValues($uri, $values)
    {
        $cookieJar = new CookieJar();
        $cookieJar->set($cookie1 = new Cookie('bar_nothing', 'bar'));
        $cookieJar->set($cookie2 = new Cookie('bar_expired', 'bar', time() - 86400));
        $cookieJar->set($cookie3 = new Cookie('bar_path', 'bar', null, '/bar'));
        $cookieJar->set($cookie4 = new Cookie('bar_domain', 'bar', null, '/', '.example.com'));
        $cookieJar->set($cookie4 = new Cookie('bar_strict_domain', 'bar', null, '/', '.www4.example.com'));
        $cookieJar->set($cookie5 = new Cookie('bar_secure', 'bar', null, '/', '', true));

        $this->assertEquals($values, array_keys($cookieJar->allValues($uri)), '->allValues() returns the cookie for a given URI');
    }

    public function provideAllValuesValues()
    {
        return array(
            array('http://www.example.com', array('bar_nothing', 'bar_domain')),
            array('http://www.example.com/', array('bar_nothing', 'bar_domain')),
            array('http://bar.example.com/', array('bar_nothing', 'bar_domain')),
            array('http://bar.example1.com/', array('bar_nothing')),
            array('https://bar.example.com/', array('bar_nothing', 'bar_secure', 'bar_domain')),
            array('http://www.example.com/bar/bar', array('bar_nothing', 'bar_path', 'bar_domain')),
            array('http://www4.example.com/', array('bar_nothing', 'bar_domain', 'bar_strict_domain')),
        );
    }

    public function testEncodedValues()
    {
        $cookieJar = new CookieJar();
        $cookieJar->set($cookie = new Cookie('bar', 'bar%3Dbaz', null, '/', '', false, true, true));

        $this->assertEquals(array('bar' => 'bar=baz'), $cookieJar->allValues('/'));
        $this->assertEquals(array('bar' => 'bar%3Dbaz'), $cookieJar->allRawValues('/'));
    }

    public function testCookieExpireWithSameNameButDifferentPaths()
    {
        $cookieJar = new CookieJar();
        $cookieJar->set($cookie1 = new Cookie('bar', 'bar1', null, '/bar'));
        $cookieJar->set($cookie2 = new Cookie('bar', 'bar2', null, '/bar'));
        $cookieJar->expire('bar', '/bar');

        $this->assertNull($cookieJar->get('bar'), '->get() returns null if the cookie is expired');
        $this->assertEquals(array(), array_keys($cookieJar->allValues('http://example.com/')));
        $this->assertEquals(array(), $cookieJar->allValues('http://example.com/bar'));
        $this->assertEquals(array('bar' => 'bar2'), $cookieJar->allValues('http://example.com/bar'));
    }

    public function testCookieExpireWithNullPaths()
    {
        $cookieJar = new CookieJar();
        $cookieJar->set($cookie1 = new Cookie('bar', 'bar1', null, '/'));
        $cookieJar->expire('bar', null);

        $this->assertNull($cookieJar->get('bar'), '->get() returns null if the cookie is expired');
        $this->assertEquals(array(), array_keys($cookieJar->allValues('http://example.com/')));
    }

    public function testCookieWithSameNameButDifferentPaths()
    {
        $cookieJar = new CookieJar();
        $cookieJar->set($cookie1 = new Cookie('bar', 'bar1', null, '/bar'));
        $cookieJar->set($cookie2 = new Cookie('bar', 'bar2', null, '/bar'));

        $this->assertEquals(array(), array_keys($cookieJar->allValues('http://example.com/')));
        $this->assertEquals(array('bar' => 'bar1'), $cookieJar->allValues('http://example.com/bar'));
        $this->assertEquals(array('bar' => 'bar2'), $cookieJar->allValues('http://example.com/bar'));
    }

    public function testCookieWithSameNameButDifferentDomains()
    {
        $cookieJar = new CookieJar();
        $cookieJar->set($cookie1 = new Cookie('bar', 'bar1', null, '/', 'bar.example.com'));
        $cookieJar->set($cookie2 = new Cookie('bar', 'bar2', null, '/', 'bar.example.com'));

        $this->assertEquals(array(), array_keys($cookieJar->allValues('http://example.com/')));
        $this->assertEquals(array('bar' => 'bar1'), $cookieJar->allValues('http://bar.example.com/'));
        $this->assertEquals(array('bar' => 'bar2'), $cookieJar->allValues('http://bar.example.com/'));
    }

    public function testCookieGetWithSubdomain()
    {
        $cookieJar = new CookieJar();
        $cookieJar->set($cookie1 = new Cookie('bar', 'bar', null, '/', '.example.com'));
        $cookieJar->set($cookie2 = new Cookie('bar1', 'bar', null, '/', 'test.example.com'));

        $this->assertEquals($cookie1, $cookieJar->get('bar', '/', 'bar.example.com'));
        $this->assertEquals($cookie1, $cookieJar->get('bar', '/', 'example.com'));
        $this->assertEquals($cookie2, $cookieJar->get('bar1', '/', 'test.example.com'));
    }

    public function testCookieGetWithSubdirectory()
    {
        $cookieJar = new CookieJar();
        $cookieJar->set($cookie1 = new Cookie('bar', 'bar', null, '/test', '.example.com'));
        $cookieJar->set($cookie2 = new Cookie('bar1', 'bar1', null, '/', '.example.com'));

        $this->assertNull($cookieJar->get('bar', '/', '.example.com'));
        $this->assertNull($cookieJar->get('bar', '/bar', '.example.com'));
        $this->assertEquals($cookie1, $cookieJar->get('bar', '/test', 'example.com'));
        $this->assertEquals($cookie2, $cookieJar->get('bar1', '/', 'example.com'));
        $this->assertEquals($cookie2, $cookieJar->get('bar1', '/bar', 'example.com'));
    }

    public function testCookieWithWildcardDomain()
    {
        $cookieJar = new CookieJar();
        $cookieJar->set(new Cookie('bar', 'bar', null, '/', '.example.com'));

        $this->assertEquals(array('bar' => 'bar'), $cookieJar->allValues('http://www.example.com'));
        $this->assertEmpty($cookieJar->allValues('http://wwwexample.com'));
    }
}
