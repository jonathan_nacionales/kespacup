<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Symfony\Component\BrowserKit\Tests;

use Symfony\Component\BrowserKit\Cookie;

class CookieTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider getTestsForToFromString
     */
    public function testToFromString($cookie, $url = null)
    {
        $this->assertEquals($cookie, (string) Cookie::fromString($cookie, $url));
    }

    public function getTestsForToFromString()
    {
        return array(
            array('bar=bar; path=/'),
            array('bar=bar; path=/bar'),
            array('bar=bar; domain=google.com; path=/'),
            array('bar=bar; domain=example.com; path=/; secure', 'https://example.com/'),
            array('bar=bar; path=/; httponly'),
            array('bar=bar; domain=google.com; path=/bar; secure; httponly', 'https://google.com/'),
            array('bar=bar=baz; path=/'),
            array('bar=bar%3Dbaz; path=/'),
        );
    }

    public function testFromStringIgnoreSecureFlag()
    {
        $this->assertFalse(Cookie::fromString('bar=bar; secure')->isSecure());
        $this->assertFalse(Cookie::fromString('bar=bar; secure', 'http://example.com/')->isSecure());
    }

    /**
     * @dataProvider getExpireCookieStrings
     */
    public function testFromStringAcceptsSeveralExpiresDateFormats($cookie)
    {
        $this->assertEquals(1596185377, Cookie::fromString($cookie)->getExpiresTime());
    }

    public function getExpireCookieStrings()
    {
        return array(
            array('bar=bar; expires=Fri, 31-Jul-2020 08:49:37 GMT'),
            array('bar=bar; expires=Fri, 31 Jul 2020 08:49:37 GMT'),
            array('bar=bar; expires=Fri, 31-07-2020 08:49:37 GMT'),
            array('bar=bar; expires=Fri, 31-07-20 08:49:37 GMT'),
            array('bar=bar; expires=Friday, 31-Jul-20 08:49:37 GMT'),
            array('bar=bar; expires=Fri Jul 31 08:49:37 2020'),
            array('bar=bar; expires=\'Fri Jul 31 08:49:37 2020\''),
            array('bar=bar; expires=Friday July 31st 2020, 08:49:37 GMT'),
        );
    }

    public function testFromStringWithCapitalization()
    {
        $this->assertEquals('Foo=Bar; path=/', (string) Cookie::fromString('Foo=Bar'));
        $this->assertEquals('bar=bar; expires=Fri, 31 Dec 2010 23:59:59 GMT; path=/', (string) Cookie::fromString('bar=bar; Expires=Fri, 31 Dec 2010 23:59:59 GMT'));
        $this->assertEquals('bar=bar; domain=www.example.org; path=/; httponly', (string) Cookie::fromString('bar=bar; DOMAIN=www.example.org; HttpOnly'));
    }

    public function testFromStringWithUrl()
    {
        $this->assertEquals('bar=bar; domain=www.example.com; path=/', (string) Cookie::FromString('bar=bar', 'http://www.example.com/'));
        $this->assertEquals('bar=bar; domain=www.example.com; path=/', (string) Cookie::FromString('bar=bar', 'http://www.example.com'));
        $this->assertEquals('bar=bar; domain=www.example.com; path=/', (string) Cookie::FromString('bar=bar', 'http://www.example.com?bar'));
        $this->assertEquals('bar=bar; domain=www.example.com; path=/bar', (string) Cookie::FromString('bar=bar', 'http://www.example.com/bar/bar'));
        $this->assertEquals('bar=bar; domain=www.example.com; path=/', (string) Cookie::FromString('bar=bar; path=/', 'http://www.example.com/bar/bar'));
        $this->assertEquals('bar=bar; domain=www.myotherexample.com; path=/', (string) Cookie::FromString('bar=bar; domain=www.myotherexample.com', 'http://www.example.com/'));
    }

    public function testFromStringThrowsAnExceptionIfCookieIsNotValid()
    {
        $this->setExpectedException('InvalidArgumentException');
        Cookie::FromString('bar');
    }

    public function testFromStringThrowsAnExceptionIfCookieDateIsNotValid()
    {
        $this->setExpectedException('InvalidArgumentException');
        Cookie::FromString('bar=bar; expires=Flursday July 31st 2020, 08:49:37 GMT');
    }

    public function testFromStringThrowsAnExceptionIfUrlIsNotValid()
    {
        $this->setExpectedException('InvalidArgumentException');
        Cookie::FromString('bar=bar', 'barbar');
    }

    public function testGetName()
    {
        $cookie = new Cookie('bar', 'bar');
        $this->assertEquals('bar', $cookie->getName(), '->getName() returns the cookie name');
    }

    public function testGetValue()
    {
        $cookie = new Cookie('bar', 'bar');
        $this->assertEquals('bar', $cookie->getValue(), '->getValue() returns the cookie value');

        $cookie = new Cookie('bar', 'bar%3Dbaz', null, '/', '', false, true, true); // raw value
        $this->assertEquals('bar=baz', $cookie->getValue(), '->getValue() returns the urldecoded cookie value');
    }

    public function testGetRawValue()
    {
        $cookie = new Cookie('bar', 'bar=baz'); // decoded value
        $this->assertEquals('bar%3Dbaz', $cookie->getRawValue(), '->getRawValue() returns the urlencoded cookie value');
        $cookie = new Cookie('bar', 'bar%3Dbaz', null, '/', '', false, true, true); // raw value
        $this->assertEquals('bar%3Dbaz', $cookie->getRawValue(), '->getRawValue() returns the non-urldecoded cookie value');
    }

    public function testGetPath()
    {
        $cookie = new Cookie('bar', 'bar', 0);
        $this->assertEquals('/', $cookie->getPath(), '->getPath() returns / is no path is defined');

        $cookie = new Cookie('bar', 'bar', 0, '/bar');
        $this->assertEquals('/bar', $cookie->getPath(), '->getPath() returns the cookie path');
    }

    public function testGetDomain()
    {
        $cookie = new Cookie('bar', 'bar', 0, '/', 'bar.com');
        $this->assertEquals('bar.com', $cookie->getDomain(), '->getDomain() returns the cookie domain');
    }

    public function testIsSecure()
    {
        $cookie = new Cookie('bar', 'bar');
        $this->assertFalse($cookie->isSecure(), '->isSecure() returns false if not defined');

        $cookie = new Cookie('bar', 'bar', 0, '/', 'bar.com', true);
        $this->assertTrue($cookie->isSecure(), '->isSecure() returns the cookie secure flag');
    }

    public function testIsHttponly()
    {
        $cookie = new Cookie('bar', 'bar');
        $this->assertTrue($cookie->isHttpOnly(), '->isHttpOnly() returns false if not defined');

        $cookie = new Cookie('bar', 'bar', 0, '/', 'bar.com', false, true);
        $this->assertTrue($cookie->isHttpOnly(), '->isHttpOnly() returns the cookie httponly flag');
    }

    public function testGetExpiresTime()
    {
        $cookie = new Cookie('bar', 'bar');
        $this->assertNull($cookie->getExpiresTime(), '->getExpiresTime() returns the expires time');

        $cookie = new Cookie('bar', 'bar', $time = time() - 86400);
        $this->assertEquals($time, $cookie->getExpiresTime(), '->getExpiresTime() returns the expires time');
    }

    public function testIsExpired()
    {
        $cookie = new Cookie('bar', 'bar');
        $this->assertFalse($cookie->isExpired(), '->isExpired() returns false when the cookie never expires (null as expires time)');

        $cookie = new Cookie('bar', 'bar', time() - 86400);
        $this->assertTrue($cookie->isExpired(), '->isExpired() returns true when the cookie is expired');

        $cookie = new Cookie('bar', 'bar', 0);
        $this->assertFalse($cookie->isExpired());
    }
}
