<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Symfony\Component\Debug\Tests\FatalErrorHandler;

use Symfony\Component\Debug\Exception\FatalErrorException;
use Symfony\Component\Debug\FatalErrorHandler\UndefinedFunctionFatalErrorHandler;

class UndefinedFunctionFatalErrorHandlerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider provideUndefinedFunctionData
     */
    public function testUndefinedFunction($error, $translatedMessage)
    {
        $handler = new UndefinedFunctionFatalErrorHandler();
        $exception = $handler->handleError($error, new FatalErrorException('', 0, $error['type'], $error['file'], $error['line']));

        $this->assertInstanceof('Symfony\Component\Debug\Exception\UndefinedFunctionException', $exception);
        // class names are case insensitive and PHP/HHVM do not return the same
        $this->assertSame(strtolower($translatedMessage), strtolower($exception->getMessage()));
        $this->assertSame($error['type'], $exception->getSeverity());
        $this->assertSame($error['file'], $exception->getFile());
        $this->assertSame($error['line'], $exception->getLine());
    }

    public function provideUndefinedFunctionData()
    {
        return array(
            array(
                array(
                    'type' => 1,
                    'line' => 12,
                    'file' => 'bar.php',
                    'message' => 'Call to undefined function test_namespaced_function()',
                ),
                'Attempted to call function "test_namespaced_function" from the global namespace in bar.php line 12. Did you mean to call: "\\symfony\\component\\debug\\tests\\fatalerrorhandler\\test_namespaced_function"?',
            ),
            array(
                array(
                    'type' => 1,
                    'line' => 12,
                    'file' => 'bar.php',
                    'message' => 'Call to undefined function Foo\\Bar\\Baz\\test_namespaced_function()',
                ),
                'Attempted to call function "test_namespaced_function" from namespace "Foo\\Bar\\Baz" in bar.php line 12. Did you mean to call: "\\symfony\\component\\debug\\tests\\fatalerrorhandler\\test_namespaced_function"?',
            ),
            array(
                array(
                    'type' => 1,
                    'line' => 12,
                    'file' => 'bar.php',
                    'message' => 'Call to undefined function bar()',
                ),
                'Attempted to call function "bar" from the global namespace in bar.php line 12.',
            ),
            array(
                array(
                    'type' => 1,
                    'line' => 12,
                    'file' => 'bar.php',
                    'message' => 'Call to undefined function Foo\\Bar\\Baz\\bar()',
                ),
                'Attempted to call function "bar" from namespace "Foo\Bar\Baz" in bar.php line 12.',
            ),
        );
    }
}

function test_namespaced_function()
{
}
