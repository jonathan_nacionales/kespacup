<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Symfony\Component\HttpKernel\Tests;

use Symfony\Component\HttpKernel\UriSigner;

class UriSignerTest extends \PHPUnit_Framework_TestCase
{
    public function testSign()
    {
        $signer = new UriSigner('barbar');

        $this->assertContains('?_hash=', $signer->sign('http://example.com/bar'));
        $this->assertContains('&_hash=', $signer->sign('http://example.com/bar?bar=bar'));
    }

    public function testCheck()
    {
        $signer = new UriSigner('barbar');

        $this->assertFalse($signer->check('http://example.com/bar?_hash=bar'));
        $this->assertFalse($signer->check('http://example.com/bar?bar=bar&_hash=bar'));
        $this->assertFalse($signer->check('http://example.com/bar?bar=bar&_hash=bar&bar=bar'));

        $this->assertTrue($signer->check($signer->sign('http://example.com/bar')));
        $this->assertTrue($signer->check($signer->sign('http://example.com/bar?bar=bar')));

        $this->assertTrue($signer->sign('http://example.com/bar?bar=bar&bar=bar') === $signer->sign('http://example.com/bar?bar=bar&bar=bar'));
    }
}
