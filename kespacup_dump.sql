-- MySQL dump 10.13  Distrib 5.5.44, for Linux (x86_64)
--
-- Host: qa-epg-db001.cmmigdn8fope.us-west-2.rds.amazonaws.com    Database: ogn
-- ------------------------------------------------------
-- Server version	5.6.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `kespacup_admins`
--

DROP TABLE IF EXISTS `kespacup_admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kespacup_admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kespacup_admins`
--

LOCK TABLES `kespacup_admins` WRITE;
/*!40000 ALTER TABLE `kespacup_admins` DISABLE KEYS */;
INSERT INTO `kespacup_admins` VALUES (2,'Admin','jonathan.nacionales@azubu.com','$2y$10$nnE/GvcYECicigoh/EBf7OHZxVJtG.YGe3SL3KDoMhEOtdoW0Gpq2','2015-01-23 22:22:05','2015-11-11 19:23:58','gA6S7XRzNCeaCz4oRAKeFa2pxOfsqG6ZzqGO7vA5MrEwfk97t1LvT83Ne8uR');
/*!40000 ALTER TABLE `kespacup_admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kespacup_migrations`
--

DROP TABLE IF EXISTS `kespacup_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kespacup_migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kespacup_migrations`
--

LOCK TABLES `kespacup_migrations` WRITE;
/*!40000 ALTER TABLE `kespacup_migrations` DISABLE KEYS */;
INSERT INTO `kespacup_migrations` VALUES ('2015_01_07_034941_create_users_table',1),('2015_01_07_035159_create_votes_table',1),('2015_01_23_200614_create_admins_table',2),('2015_01_23_200735_add_payload_to_votes_table',2),('2015_01_23_200755_add_admin_to_votes_table',2),('2015_01_23_212413_add_token_to_admins_table',3),('2015_01_27_214129_create_players_table',4),('2015_01_27_214915_create_players_table',5),('2015_01_27_222021_add_image_and_video_url_to_players_table',6),('2015_01_28_002029_create_videos_table',7),('2015_02_03_000739_create_settings_table',8),('2015_02_03_052512_create_session_table',9);
/*!40000 ALTER TABLE `kespacup_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kespacup_players`
--

DROP TABLE IF EXISTS `kespacup_players`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kespacup_players` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `featured` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `image_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kespacup_players`
--

LOCK TABLES `kespacup_players` WRITE;
/*!40000 ALTER TABLE `kespacup_players` DISABLE KEYS */;
INSERT INTO `kespacup_players` VALUES (96,'Jeon Ho Jin <br /> 전호진 <br/> <strong>(Longzhu Lilac)</strong>','Longzhu',0,'2015-11-06 22:20:51','2015-11-11 02:02:59','/img/kespacup/Longzhu.png',''),(97,'Koo Bon tae k <br /> 구본택 <br/> <strong>(Longzhu Expe)</strong>','Longzhu',0,'2015-11-06 22:23:13','2015-11-11 01:12:56','/img/kespacup/Longzhu.png',''),(98,'Park Jong ik <br /> 박종익 <br/> <strong>(Longzhu Tusin)</strong>','Longzhu',0,'2015-11-06 22:29:12','2015-11-11 01:13:17','/img/kespacup/Longzhu.png',''),(99,'Kim Tae il <br /> 김태일 <br/> <strong>(Longzhu Frozen)</strong>','Longzhu',0,'2015-11-06 22:29:40','2015-11-11 01:13:25','/img/kespacup/Longzhu.png',''),(100,'Oh jang won <br /> 오장원 <br/> <strong>(Longzhu Roar)</strong>','Longzhu',0,'2015-11-06 22:30:02','2015-11-11 01:13:42','/img/kespacup/Longzhu.png',''),(101,'Son Seung ik <br /> 손승익 <br/> <strong>(Longzhu Sonstar)</strong>','Longzhu',0,'2015-11-06 22:30:24','2015-11-11 01:13:53','/img/kespacup/Longzhu.png',''),(102,'Lee Dong geun <br /> 이동근 <br/> <strong>(Longzhu Ignar)</strong>','Longzhu',0,'2015-11-06 22:30:42','2015-11-11 01:14:06','/img/kespacup/Longzhu.png',''),(103,'Min Ju Seong	<br/> 민주성 <br /> <strong>(Mimic)</strong>','CTU Pathos',0,'2015-11-06 23:00:52','2015-11-11 01:21:00','/img/kespacup/CTU_Pathos.png',''),(104,'Kim Gyeong Tak<br/> 김경탁 <br/> 	<strong> (Dokgo) </strong>','CTU Pathos',0,'2015-11-06 23:02:25','2015-11-11 02:03:02','/img/kespacup/CTU_Pathos.png',''),(105,'Ko Jae Wook<br /> 고재욱 <br/>	<strong>(Mangchi)</strong>','CTU Pathos',0,'2015-11-06 23:02:48','2015-11-11 01:21:26','/img/kespacup/CTU_Pathos.png',''),(106,'Ryu Seon Wu <br/> 유선우 <br/>	<strong>(GAP)</strong>','CTU Pathos',0,'2015-11-06 23:03:28','2015-11-11 01:21:38','/img/kespacup/CTU_Pathos.png',''),(107,' Kim Tae Gyeom <br/> 김태겸 <br /> <strong>(Mocha)</strong>','CTU Pathos',0,'2015-11-06 23:03:52','2015-11-11 01:21:50','/img/kespacup/CTU_Pathos.png',''),(108,'RYu Se Hun <br/> 유세훈 <br/>	<strong>(Befree)</strong>','CTU Pathos',0,'2015-11-06 23:04:17','2015-11-11 01:22:00','/img/kespacup/CTU_Pathos.png',''),(109,'Kim Chung Hwan <br/> 김청환 <br/>	<strong>(Beggar)</strong>','Young Boss',0,'2015-11-06 23:05:56','2015-11-11 01:22:19','/img/kespacup/Young_Boss.png',''),(110,'Lee Dong su <br/>	이동수	<br/> <strong>(NonStop)</strong>','Young Boss',0,'2015-11-11 01:24:32','2015-11-11 20:46:57','/img/kespacup/Young_Boss.png',''),(111,'Seok Hyeon jun <br/>	석현준	<br/> <strong>(Hipo)</strong>','Young Boss',0,'2015-11-11 01:25:10','2015-11-11 20:47:09','/img/kespacup/Young_Boss.png',''),(112,'Lee Dae Hyung	<br/> 이대형	<br/> <strong>(19)</strong>','Young Boss',0,'2015-11-11 01:25:35','2015-11-11 20:47:21','/img/kespacup/Young_Boss.png',''),(113,'Ha Jonghun	<br/> 하종훈 <br/>	<strong>(Kramer)</strong>','Young Boss',0,'2015-11-11 01:25:58','2015-11-11 20:47:32','/img/kespacup/Young_Boss.png',''),(114,'Jin gyeong hwan <br/>	진경환	<br/> <strong>(Hide)</strong>','Young Boss',0,'2015-11-11 01:26:27','2015-11-11 20:47:44','/img/kespacup/Young_Boss.png',''),(115,'Kim jae hee	<br/> 김재희	<br/> <strong>(ESC Ever Crazy)</strong>','ESC Ever',0,'2015-11-11 01:27:09','2015-11-11 20:48:52','/img/kespacup/ESC_Ever.png',''),(116,'Kim min kwon	<br/> 김민권	<br/> <strong>(ESC Ever Ares)</strong>','ESC Ever',0,'2015-11-11 01:27:34','2015-11-11 20:49:18','/img/kespacup/ESC_Ever.png',''),(117,'Lee jun seok	<br/> 이준석	<br/> <strong>(ESC Ever Ryan)</strong>','ESC Ever',0,'2015-11-11 01:28:04','2015-11-11 20:49:30','/img/kespacup/ESC_Ever.png',''),(118,'Kang ha woon	<br/> 강하운	<br/> <strong>ESC Ever Athena</strong>','ESC Ever',0,'2015-11-11 01:28:29','2015-11-11 20:49:42','/img/kespacup/ESC_Ever.png',''),(119,'Park hyeong gi	<br/> 박형기	<br/> <strong>(ESC Ever Police)</strong>','ESC Ever',0,'2015-11-11 01:28:52','2015-11-11 20:49:55','/img/kespacup/ESC_Ever.png',''),(120,'Lee dong wook	<br/> 이동욱	<br/> <strong>(ESC Ever LokeN)</strong>','ESC Ever',0,'2015-11-11 01:29:13','2015-11-11 20:50:13','/img/kespacup/ESC_Ever.png',''),(121,'Kim han gi	<br/> 김한기	<br/> <strong>(ESC Ever KeY)</strong>','ESC Ever',0,'2015-11-11 01:29:36','2015-11-11 20:50:36','/img/kespacup/ESC_Ever.png',''),(122,'Eun jong seop	<br/> 은종섭	<br/> <strong>(ESC Ever Totoro)</strong>','ESC Ever',0,'2015-11-11 01:30:02','2015-11-11 20:51:26','/img/kespacup/ESC_Ever.png',''),(123,'Seo Hyun-seok	<br/> 서현석	<br/> <strong>(SSB Soul)</strong>','SBENU',0,'2015-11-11 01:30:38','2015-11-11 21:50:03','/img/kespacup/SBENU.png',''),(124,'Lee Kang-pyo	<br/> 이강표	<br/> <strong>(SSB Soar)</strong>','SBENU',0,'2015-11-11 01:31:08','2015-11-11 21:50:20','/img/kespacup/SBENU.png',''),(125,'Yoon Sang-ho	<br/> 윤상호	<br/> <strong>(SSB Catch)</strong>','SBENU',0,'2015-11-11 01:31:37','2015-11-11 21:50:29','/img/kespacup/SBENU.png',''),(126,'Sung Yeon-jun 	<br/> 성연준	<br/> <strong>(SSB Flawless)</strong>','SBENU',0,'2015-11-11 01:32:06','2015-11-11 21:50:40','/img/kespacup/SBENU.png',''),(127,'Oh Seung-ju	<br/> 오승주	<br/> <strong>(SSB Sasin)</strong>','SBENU',0,'2015-11-11 01:32:31','2015-11-11 21:50:51','/img/kespacup/SBENU.png',''),(128,'Shin Jung-hyun	<br/> 신정현	<br/> <strong>(SSB Nuclear)</strong>','SBENU',0,'2015-11-11 01:32:58','2015-11-11 21:51:02','/img/kespacup/SBENU.png',''),(129,'Park Ki-sun	<br/> 박기선	<br/> <strong>(SSB Secret)</strong>','SBENU',0,'2015-11-11 01:33:24','2015-11-11 21:51:15','/img/kespacup/SBENU.png',''),(130,'jeon iksu	<br/>전익수	<br/> <strong>(anc Ikssu)</strong>','Anarchy',0,'2015-11-11 01:34:45','2015-11-11 21:51:41','/img/kespacup/Anarchy.png',''),(131,'nam tae yoo	<br/> 남태유	<br/> <strong>anc Lira</strong>','Anarchy',0,'2015-11-11 01:35:08','2015-11-11 21:52:01','/img/kespacup/Anarchy.png',''),(132,'son young min	 <br/> 손영민	<br/> <strong>(anc Mickey)</strong>','Anarchy',0,'2015-11-11 01:35:36','2015-11-11 21:52:22','/img/kespacup/Anarchy.png',''),(133,'kwon sang yun	<br/> 권상윤	<br/> <strong>(anc Sangyun)</strong>','Anarchy',0,'2015-11-11 01:35:56','2015-11-11 21:52:34','/img/kespacup/Anarchy.png',''),(134,'No Hoei Jong	<br/> 노회종	<br/> <strong>(anc Snowflower)</strong>','Anarchy',0,'2015-11-11 01:36:20','2015-11-11 21:52:44','/img/kespacup/Anarchy.png',''),(135,'Yeo Chang Dong	<br/> 여창동	<br/> <strong>(Jin Air TrAce)</strong>','JIN AIR',0,'2015-11-11 01:37:01','2015-11-11 21:56:53','/img/kespacup/JIN_AIR.png',''),(136,'Kim Jun Yeong	<br/> 김준영	<br/> <strong>(Jin Air SoHwan)</strong>','JIN AIR',0,'2015-11-11 01:37:34','2015-11-11 21:57:21','/img/kespacup/JIN_AIR.png',''),(137,'Lee Sang Hyun	<br/> 이상현	<br/> <strong>(Jin Air Chaser)</strong>','JIN AIR',0,'2015-11-11 01:37:57','2015-11-11 21:57:32','/img/kespacup/JIN_AIR.png',''),(138,'Park Tae Jin	<br/> 박태진	<br/> <strong>(Jin Air Winged)</strong>','JIN AIR',0,'2015-11-11 01:38:21','2015-11-11 21:57:44','/img/kespacup/JIN_AIR.png',''),(139,'Lee Seong Hyuk	<br/> 이성혁	<br/> <strong>(Jin Air Kuzan)</strong>','JIN AIR',0,'2015-11-11 01:38:44','2015-11-11 21:57:55','/img/kespacup/JIN_AIR.png',''),(140,' Kang Hyung Woo	<br/> 강형우	<br/> <strong>(Jin Air Cpt Jack)</strong>','JIN AIR',0,'2015-11-11 01:39:06','2015-11-11 21:58:05','/img/kespacup/JIN_AIR.png',''),(141,'Na Woo Hyung	<br/> 나우형	<br/> <strong>(Jin Air Pilot)</strong>','JIN AIR',0,'2015-11-11 01:39:29','2015-11-11 21:58:15','/img/kespacup/JIN_AIR.png',''),(142,'Choi Sun Ho	<br/> 최선호	<br/> <strong>(Jin Air Chei)</strong>','JIN AIR',0,'2015-11-11 01:39:51','2015-11-11 21:58:28','/img/kespacup/JIN_AIR.png',''),(143,'Lee Eun Teck	<br/> 이은택	<br/> <strong>(Jin Air Sweet)</strong>','JIN AIR',0,'2015-11-11 01:40:12','2015-11-11 21:58:39','/img/kespacup/JIN_AIR.png',''),(144,'Park sangmyeon	<br/> 박상면	<br/> <strong>(CJE Shy)</strong>','CJ',0,'2015-11-11 01:40:50','2015-11-11 21:59:01','/img/kespacup/CJ.png',''),(145,'Kang chanyong	<br/> 강찬용	<br/> <strong>(CJE Ambition)</strong>','CJ',0,'2015-11-11 01:41:13','2015-11-11 21:59:18','/img/kespacup/CJ.png',''),(146,'Shin jinyoung	<br/> 신진영	<br/> <strong>(CJE Coco)</strong>','CJ',0,'2015-11-11 01:41:37','2015-11-11 21:59:28','/img/kespacup/CJ.png',''),(147,'Sun Hosan	<br/> 선호산	<br/> <strong>(CJE Space)</strong>','CJ',0,'2015-11-11 01:42:34','2015-11-11 21:59:40','/img/kespacup/CJ.png',''),(148,'Hong mingi	<br/> 홍민기	<br/> <strong>(CJE Madlife)</strong>','CJ',0,'2015-11-11 01:42:58','2015-11-11 22:00:00','/img/kespacup/CJ.png',''),(149,'JANG GYEONGHWAN	<br/> 장경환	<br/> <strong>(SKT Marin)</strong>','SK Telecom',0,'2015-11-11 01:43:35','2015-11-11 22:00:29','/img/kespacup/SK_Telecom.png',''),(150,'BAE SEONGUNG	<br/> 배성웅	<br/> <strong>SKT Bengi</strong>','SK Telecom',0,'2015-11-11 01:44:08','2015-11-11 22:00:46','/img/kespacup/SK_Telecom.png',''),(151,'IM JAEHYEON <br/>	임재현	<br/> <strong>(SKT Tom)</strong>','SK Telecom',0,'2015-11-11 01:44:29','2015-11-11 22:00:59','/img/kespacup/SK_Telecom.png',''),(152,'LEE JIHOON	<br/> 이지훈	<br/> <strong>(SKT Easyhoon)</strong>','SK Telecom',0,'2015-11-11 01:44:53','2015-11-11 22:01:10','/img/kespacup/SK_Telecom.png',''),(153,'LEE SANGHYEOK	<br/>이상혁	<br/> <strong>(SKT Faker)</strong>','SK Telecom',0,'2015-11-11 01:45:15','2015-11-11 22:01:21','/img/kespacup/SK_Telecom.png',''),(154,'LEE YECHAN	<br/> 이예찬	<br/> <strong>(SKT Scout)</strong>','SK Telecom',0,'2015-11-11 01:45:39','2015-11-11 22:01:33','/img/kespacup/SK_Telecom.png',''),(155,'BAE JUNSIK	<br/> 배준식	<br/> <strong>(SKT Bang)</strong>','SK Telecom',0,'2015-11-11 01:46:01','2015-11-11 22:01:44','/img/kespacup/SK_Telecom.png',''),(156,'LEE JAEWAN	<br/> 이재완	<br/> <strong>(SKT Wolf)</strong>','SK Telecom',0,'2015-11-11 01:46:23','2015-11-11 22:01:57','/img/kespacup/SK_Telecom.png',''),(157,'Lee HoSeong	<br/> 이호성	<br/> <strong>(NJF Duke)</strong>','Najin',0,'2015-11-11 01:47:07','2015-11-11 22:38:36','/img/kespacup/Najin.png',''),(158,'Cho jaegeol	<br/> 조재걸	<br/> <strong>(NJF Watch)</strong>','Najin',0,'2015-11-11 01:47:30','2015-11-11 22:38:57','/img/kespacup/Najin.png',''),(159,'Yoon wangho	<br/> 윤왕호	<br/> <strong>(NJF Peanut)</strong>','Najin',0,'2015-11-11 01:47:53','2015-11-11 22:39:12','/img/kespacup/Najin.png',''),(160,'Yoo byeongjun	<br/>유병준	<br/> <strong>(NJF Ggoong)</strong>','Najin',0,'2015-11-11 01:48:17','2015-11-11 22:39:27','/img/kespacup/Najin.png',''),(161,'Park danwon	<br/> 박단원	<br/> <strong>(NJF TANK)</strong>','Najin',0,'2015-11-11 01:48:41','2015-11-11 22:39:42','/img/kespacup/Najin.png',''),(162,'Oh gyumin	<br/>오규민	<br/> <strong>(NJF Ohq)</strong>','Najin',0,'2015-11-11 01:49:04','2015-11-11 22:39:53','/img/kespacup/Najin.png',''),(163,'Jang nuri	 <br/> 장누리	<br/> <strong>(NJF Cain)</strong>','Najin',0,'2015-11-11 01:49:25','2015-11-11 22:40:15','/img/kespacup/Najin.png',''),(164,'Kim jinseon	<br/> 김진선	<br/> <strong>(NJF Pure)</strong>','Najin',0,'2015-11-11 01:50:07','2015-11-11 22:40:29','/img/kespacup/Najin.png',''),(165,'Lee sung jin	<br/> 이성진	<br/> <strong>(SSG Cuvee)</strong>','SAMSUNG',0,'2015-11-11 01:51:03','2015-11-11 22:41:12','/img/kespacup/SAMSUNG.png',''),(166,'Lee Min Ho	<br/> 이민호	<br/> <strong>(SSG Crown)</strong>','SAMSUNG',0,'2015-11-11 01:51:27','2015-11-11 22:41:30','/img/kespacup/SAMSUNG.png',''),(167,'Kim Ji Hoon	<br/> 김지훈	<br/> <strong>(SSG ACE)</strong>','SAMSUNG',0,'2015-11-11 01:51:49','2015-11-11 22:41:43','/img/kespacup/SAMSUNG.png',''),(168,'Seo Jun Cheol	 <br/> 서준철	<br/> <strong>(SSG Eve)</strong>','SAMSUNG',0,'2015-11-11 01:52:16','2015-11-11 22:41:56','/img/kespacup/SAMSUNG.png',''),(169,'Park Jong Won	<br/> 박종원	<br/> <strong>(SSG Bliss)</strong>','SAMSUNG',0,'2015-11-11 01:53:05','2015-11-11 22:42:09','/img/kespacup/SAMSUNG.png',''),(170,'Lee Jin Yong	<br/> 이진용	<br/> <strong>(SSG Fury)</strong>','SAMSUNG',0,'2015-11-11 01:53:26','2015-11-11 22:42:22','/img/kespacup/SAMSUNG.png',''),(171,'Kwon Ji Min	<br/> 권지민	<br/> <strong>(SSG Wraith)</strong>','SAMSUNG',0,'2015-11-11 01:53:53','2015-11-11 22:42:35','/img/kespacup/SAMSUNG.png',''),(172,'Jang Kyung Ho	<br/> 장경호	<br/> <strong>(SSG Luna)</strong>','SAMSUNG',0,'2015-11-11 01:54:22','2015-11-11 22:42:45','/img/kespacup/SAMSUNG.png',''),(173,'KIM CHAN HO	 <br/>김찬호	<br/> <strong>(kt Ssumday)</strong>','KT',0,'2015-11-11 01:55:08','2015-11-11 22:43:24','/img/kespacup/KT.png',''),(174,'GO DONG BIN	<br/> 고동빈	<br/> <strong>(kt Score)</strong>','KT',0,'2015-11-11 01:55:31','2015-11-11 22:43:46','/img/kespacup/KT.png',''),(175,'KIM SANG MOON	<br/> 김상문	<br/> <strong>(kt Nagne)</strong>','KT',0,'2015-11-11 01:55:52','2015-11-11 22:43:58','/img/kespacup/KT.png',''),(176,'NOH DONG HYEON	<br/> 노동현	<br/> <strong>(kt Arrow)</strong>','KT',0,'2015-11-11 01:56:15','2015-11-11 22:44:16','/img/kespacup/KT.png',''),(177,'LEE JONG BEOM	<br/> 이종범	<br/> <strong>(kt Piccaboo)</strong>','KT',0,'2015-11-11 01:56:42','2015-11-11 22:44:27','/img/kespacup/KT.png',''),(178,'JEONG JAE WOO	<br/> 정재우	<br/> <strong>(kt Fixer)</strong>','KT',0,'2015-11-11 01:57:04','2015-11-11 22:44:41','/img/kespacup/KT.png',''),(179,'LEE HO SUNG	 <br/> 이호성	<br/> <strong>(kt Edge)</strong>','KT',0,'2015-11-11 01:57:25','2015-11-11 22:44:53','/img/kespacup/KT.png',''),(180,'OH HYUN SIK	<br/> 오현식	<br/> <strong>(kt Mach)</strong>','KT',0,'2015-11-11 01:58:04','2015-11-11 22:45:04','/img/kespacup/KT.png',''),(181,'jung jae woo	<br/>정재우	<br/> <strong>(Wns bokgu)</strong>','Winners',0,'2015-11-11 01:58:46','2015-11-11 22:48:15','/img/kespacup/Winners.png',''),(182,'kim seong hwan	<br/> 김성환	<br/> <strong>(Wns GankZero)</strong>','Winners',0,'2015-11-11 01:59:08','2015-11-11 22:51:21','/img/kespacup/Winners.png',''),(183,'mun ji won	<br/> 문지원	<br/> <strong>(Wns Murphy)</strong>','Winners',0,'2015-11-11 01:59:31','2015-11-11 22:51:37','/img/kespacup/Winners.png',''),(184,'na gun woo	<br/> 나건우	<br/> <strong>(Wns Night)</strong>','Winners',0,'2015-11-11 01:59:53','2015-11-11 23:04:18','/img/kespacup/Winners.png',''),(185,'kim tae Hyoung	<br/> 김태형	<br/> <strong>(Wns honeyTeng)</strong>','Winners',0,'2015-11-11 02:00:14','2015-11-11 23:04:55','/img/kespacup/Winners.png',''),(186,'Lee ho jin	 <br/>이호진	<br/> <strong>(KOO Hojin)</strong>','Tigers',0,'2015-11-11 02:00:47','2015-11-11 23:08:25','/img/kespacup/Tigers.png',''),(187,'Kim jong in	<br/> 김종인	<br/> <strong>(KOO PraY)</strong>','Tigers',0,'2015-11-11 02:01:09','2015-11-11 23:09:30','/img/kespacup/Tigers.png',''),(188,'Lee seo haeng	 <br/> 이서행	<br/> <strong>(KOO Kuro)</strong>','Tigers',0,'2015-11-11 02:01:30','2015-11-11 23:09:42','/img/kespacup/Tigers.png',''),(189,'Kang beom hyun	<br/> 강범현	<br/> <strong>(KOO GorillA)</strong>','Tigers',0,'2015-11-11 02:01:51','2015-11-11 23:09:53','/img/kespacup/Tigers.png',''),(190,'Song Kyung ho	<br/> 송경호	<br/> <strong>(KOO Smeb)</strong>','Tigers',0,'2015-11-11 02:02:12','2015-11-11 23:10:04','/img/kespacup/Tigers.png',''),(191,'Kim Tae wan	<br/> 김태완	<br/> <strong>(KOO Wisdom)</strong>','Tigers',0,'2015-11-11 02:02:34','2015-11-11 23:10:14','/img/kespacup/Tigers.png','');
/*!40000 ALTER TABLE `kespacup_players` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kespacup_sessions`
--

DROP TABLE IF EXISTS `kespacup_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kespacup_sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kespacup_sessions`
--

LOCK TABLES `kespacup_sessions` WRITE;
/*!40000 ALTER TABLE `kespacup_sessions` DISABLE KEYS */;
INSERT INTO `kespacup_sessions` VALUES ('ebf68f719871c2a063fff5d21376d65c79abbe79','YTozOntzOjY6Il90b2tlbiI7czo0MDoiZjd1RXJWNnpUNmhDYTA2RFJ2a3JBTUlmVGdXVmZyY1NJdE9ldXE2ZCI7czo5OiJfc2YyX21ldGEiO2E6Mzp7czoxOiJ1IjtpOjE0NDcyODExMDY7czoxOiJjIjtpOjE0NDcyODExMDY7czoxOiJsIjtzOjE6IjAiO31zOjU6ImZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=',1447281106),('fec0e54053a033966aaa82fd1fcf60bf476ede1a','YTo0OntzOjY6Il90b2tlbiI7czo0MDoiNXBwUzg5bWFYWFVGV1kyYk5vNTlpZDhhUUEwTm1QdjFqYjNkcmhOViI7czo1OiJmbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjM4OiJsb2dpbl84MmU1ZDJjNTZiZGQwODExMzE4ZjBjZjA3OGI3OGJmYyI7czoxOiIyIjtzOjk6Il9zZjJfbWV0YSI7YTozOntzOjE6InUiO2k6MTQ0NzI4MzQ2NTtzOjE6ImMiO2k6MTQ0NzI2OTIyOTtzOjE6ImwiO3M6MToiMCI7fX0=',1447283466);
/*!40000 ALTER TABLE `kespacup_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kespacup_settings`
--

DROP TABLE IF EXISTS `kespacup_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kespacup_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `setting` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kespacup_settings`
--

LOCK TABLES `kespacup_settings` WRITE;
/*!40000 ALTER TABLE `kespacup_settings` DISABLE KEYS */;
INSERT INTO `kespacup_settings` VALUES (1,'voting_enabled','true','2015-02-03 00:13:25','2015-11-06 22:23:46');
/*!40000 ALTER TABLE `kespacup_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kespacup_teams`
--

DROP TABLE IF EXISTS `kespacup_teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kespacup_teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `enabled` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kespacup_teams`
--

LOCK TABLES `kespacup_teams` WRITE;
/*!40000 ALTER TABLE `kespacup_teams` DISABLE KEYS */;
INSERT INTO `kespacup_teams` VALUES (1,'No Team','this_is_an_image.png',0,'2015-06-01 21:54:08','2015-06-01 22:00:11'),(13,'Longzhu','',1,'2015-11-06 22:07:57','2015-11-06 22:07:57'),(14,'CTU Pathos','',1,'2015-11-06 22:08:03','2015-11-06 22:08:03'),(15,'Young Boss','',1,'2015-11-06 22:08:08','2015-11-06 22:08:08'),(16,'ESC Ever','',1,'2015-11-06 22:08:13','2015-11-06 22:08:13'),(17,'SBENU','',1,'2015-11-06 22:08:18','2015-11-06 22:08:18'),(18,'Anarchy','',1,'2015-11-06 22:08:22','2015-11-06 22:08:22'),(19,'JIN AIR','',1,'2015-11-06 22:08:27','2015-11-06 22:08:27'),(20,'CJ','',1,'2015-11-06 22:08:30','2015-11-06 22:08:30'),(21,'SK Telecom','',1,'2015-11-06 22:08:35','2015-11-06 22:08:35'),(22,'Najin','',1,'2015-11-06 22:08:40','2015-11-06 22:08:40'),(23,'SAMSUNG','',1,'2015-11-06 22:08:50','2015-11-06 22:08:50'),(24,'KT','',1,'2015-11-06 22:08:53','2015-11-06 22:08:53'),(25,'Winners','',1,'2015-11-06 22:08:56','2015-11-06 22:08:56'),(26,'Tigers','',1,'2015-11-06 22:09:00','2015-11-06 22:09:00');
/*!40000 ALTER TABLE `kespacup_teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kespacup_users`
--

DROP TABLE IF EXISTS `kespacup_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kespacup_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kespacup_users`
--

LOCK TABLES `kespacup_users` WRITE;
/*!40000 ALTER TABLE `kespacup_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `kespacup_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kespacup_videos`
--

DROP TABLE IF EXISTS `kespacup_videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kespacup_videos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=150 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kespacup_videos`
--

LOCK TABLES `kespacup_videos` WRITE;
/*!40000 ALTER TABLE `kespacup_videos` DISABLE KEYS */;
/*!40000 ALTER TABLE `kespacup_videos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kespacup_votes`
--

DROP TABLE IF EXISTS `kespacup_votes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kespacup_votes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `vote` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `payload` int(10) unsigned NOT NULL DEFAULT '1',
  `admin` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kespacup_votes`
--

LOCK TABLES `kespacup_votes` WRITE;
/*!40000 ALTER TABLE `kespacup_votes` DISABLE KEYS */;
/*!40000 ALTER TABLE `kespacup_votes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-11 23:16:06
