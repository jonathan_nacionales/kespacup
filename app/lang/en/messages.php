<?php

return array(
    'welcome' => '<strong>Welcome</strong> to <em>The Azubu Super Play Award</em> voting page!',
    'briefing'=> 'Cast your vote here for who you think made the biggest play this week. Here\'s how it works:',
    'point_1' => 'Every week, come to this page to cast your vote for the player whom you believe was the top player of the week.',
    'point_2' => 'You can cast a maximum of one (1) vote every 24 hours.',
    'point_3' => 'Once you\'ve cast your vote, you won\'t be able to vote again until the 24-hour lockout is lifted.',
    'vote'    => 'VOTE',
    'error'   => 'ERROR',
    'success' => 'SUCCESS',
    'success_message' => 'Thanks for voting!',
    'error_too_soon' => 'Please wait 24 hours before voting again.',
    'error_system'  => 'Could not process request. Refresh and try again.',
    'close' => 'close',
    'voting_closes' => 'Voting closes on Monday at 24:00 KST / 7:00 PST and reopens on Wednesday at 18:00 KST / 01:00 PST.',
    'point_4' => 'OGN Admins will also place one vote per day, and their votes will add up to 30% of the total vote.',
    'point_5' => 'Players may not be chosen for this award more than two weeks in a row.',
    'results_down' => 'The result page was inaccessible for a short period of time due to construction, but should now be properly accessible.',
    'candidates' => 'Candidates',
//    'point' => 'After each LCK day, players who have made a great impact on their games will be chosen as candidates.',
   'point' => '
<p><strong>2015 LoL KeSPA Cup Azubu MVP Awards</strong></p>
<p>
An open tournament with amateurs and pros together! Which team will be the winner for 2015 Lol KeSPA Cup? Pick your KeSPA Cup Azubu MVP with your own hand!
</p>
<p>
The vote will start from Nov 14th 18:30 KST (GMT +9) and finish when the last game ends. The candidates are all the players on the finals! You can only vote once!
</p>
<p>
*The Journalist vote 50% + Fan vote 50%
</p>
<p><strong>Selection Method/Standard</strong></p>
<p>
- MVP Selection : The Journalist vote 50% + Azubu Online vote 50%<br/>
- Time : 11/14(Sat) 18:30 KST (GMT +9) ~ The end of the last game<br/>
- Candidates : All the players on the finals<br/>
- One person can vote only once <br/>
</p>

   ',
   'vote_message' => 'Vote for the Azubu MVP'
);
