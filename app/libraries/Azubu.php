<?php


class Azubu {


    /*
     *
     *  Get the user's IP address
     *
     *  @returns string (as md5 hash)
     *
     */

    public static function get_user_ip() {

        /*
         *  Set const. IP for local development
         */
        if ( App::environment('local') ):

            return 'development';

        endif;


        /*
         *
         *  Grab IP forwarded from proxy (Will cross examine IP with jQuery Library)
         *
         */
        if(isset($_SERVER["HTTP_X_FORWARDED_FOR"]) AND $_SERVER["HTTP_X_FORWARDED_FOR"] != ""):

            return md5( $_SERVER["HTTP_X_FORWARDED_FOR"] );

        else:

            return md5( $_SERVER["REMOTE_ADDR"] );

        endif;


    }


    /*
     *
     *  Add a vote
     *
     *  @returns vote model
     *
     */	

    public static function add_vote( $user_id , $player, $payload = 1, $admin = 0) {
        $vote = new Vote;
        $vote->user_id = $user_id;
        $vote->vote = $player;
		
		if( $payload > 1 ):
			$vote->payload = $payload;
			$vote->admin = $admin;
		endif;
		
        $vote->save();
        return $player;
    }

	
    /*
     *
     *  Get Total Votes
     *
     *  @returns integer
     *
     */
	public static function get_total_votes() {
	
		$count = 0;
		
		$votes = Vote::all();
		
		foreach( $votes as $vote ):
		
			$count = $count + $vote->payload;
		
		endforeach;
		
		return $count;
	
	}


	
	
	
	
	
	public static function voting_enabled() {

		$s = Setting::where('setting','=','voting_enabled')->first();

		if( $s->value == 'true' ):
			return true;
		else:
			return false;
		endif;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}