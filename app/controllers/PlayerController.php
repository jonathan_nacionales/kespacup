<?php

class PlayerController extends BaseController {

	public function index() {
		$players = Player::all();
		return View::make('player.index', array('players' => $players));
	}


	public function getCreate() {
      $results = DB::select( DB::raw("
         SELECT
            UPPER(t.name) as name,
            t.name as value
         FROM kespacup_teams t 
      "));

      $teams = array();

      foreach( $results as $result ) {
         $team = new stdclass();
         $team->value = $result->value;
         $team->name = $result->name;

         $teams[] = $team;
      }

		return View::make('player.create', array( 'teams' => $teams ));

	}


	public function postCreate() {

		$name = Input::get('name');
		$team = Input::get('team');
		$image_url = Input::get('image_url');

		if( !$name ):
			Return Redirect::to('players/create')->with('message', 'Name is required');
		elseif( !$team ):
			Return Redirect::to('players/create')->with('message', 'Team is required');
		else:

			$player = new Player;
			$player->name = $name;
			$player->team = $team;
			$player->image_url = $image_url;
			$player->save();

			Return Redirect::to('players/create')->with('message_success', 'Player Created!');;

		endif;

	}


	public function getEdit( $id ) {

		$player = Player::find( $id );

      $teams = array();

      $results = DB::select( DB::raw("
         SELECT
            UPPER(t.name) as name,
            t.name as value
         FROM kespacup_teams t 
      "));

      foreach( $results as $result ) {
         $team = new stdclass();
         $team->selected = 
            ($player->team === $result->value) ?
            "selected" :
            "";
         $team->value = $result->value;
         $team->name = $result->name;

         $teams[] = $team;
      }



		return View::make('player.edit', array(
         'player' => $player,
         'teams' => $teams ));


	}


	public function postEdit( $id ) {

		$name =        Input::get('name');
		$team =        Input::get('team');
		$image_url =   Input::get('image_url');
		$video_url =   Input::get('video_url');
		$featured =    Input::get('featured');

		if( !$name ):
			Return Redirect::to('players/'.$id.'/edit')->with('message', 'Name is required');
		elseif( !$team ):
			Return Redirect::to('players/'.$id.'/edit')->with('message', 'Team is required');
		else:

			$player = Player::find($id);
			$player->name = $name;
			$player->team = $team;
			$player->image_url = $image_url;
			$player->video_url = $video_url;
			$player->featured = $featured;
			$player->save();

			Return Redirect::to('players/'.$id.'/edit')->with('message_success', 'Player Edited!');;

		endif;


	}



	public function postFeature() {

		if( Request::ajax() ):
			$player = Input::get('player_id');
			$status = Input::get('status');

			$player = Player::find($player);

			if( $status == 1 )
				$player->featured = 0;

			if( $status == 0 )
				$player->featured = 1;

			$player->save();

			return Response::json(array('success' => true));
		endif;

		return Response::json(array('success' => false));


	}






	
}
