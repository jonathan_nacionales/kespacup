<?php

class SettingsController extends BaseController {


	public function index() {

		return View::make('settings.index');

	}

	public function postSettings() {

		$s = Setting::where('setting','=','voting_enabled')->first();
		$s->value = Input::get('voting_enabled');
		$s->save();
		return Redirect::to('settings')->with('message_success','Saved');

	}

}