<?php

class TeamController extends BaseController {
	

	public function index() {
		$teams = Team::all();
		return View::make('team.index', array('teams' => $teams));
	}

	public function getCreate() {

		return View::make('team.create');
	}
   
	public function postCreate() {
      $name = Input::get("name");
      $image_url = Input::get("image_url");
      $enabled = Input::get("enabled");

		if( !$name ):
			Return Redirect::to('players/create')->with('message', 'Name is required');
		else:
         $team = new Team;
         $team->name = $name;
         $team->image_url = $image_url;
         $team->enabled = $enabled;
         $team->save();

         return Redirect::to("teams/create")
            ->with("message_success", "Player Created!");
      endif;
	}


	public function postReset() {

      return Redirect::to('team.index')->with('message_success', 'Votes
      Reset!');;
	}

   public function getEdit($id) {
      $team = Team::find($id);
   
      return View::make('team.edit', array('team' => $team));
   }

   public function postEdit($id) {
      $name = Input::get('name');
      $image_url = Input::get('image_url');
      $enabled = Input::get('enabled');

		if( !$name ):
			Return Redirect::to('players/'.$id.'/edit')->with('message', 'Name is required');
		elseif( !$image_url ):
			Return Redirect::to('players/'.$id.'/edit')->with('message', 'Image Url is required');
		else:

         $team = Team::find($id);
         $team->name = $name;
         $team->image_url = $image_url;
         $team->enabled = $enabled;
         $team->save();

         Return Redirect::to('teams/'.$id.'/edit')->with('message_success', 'Team Edited!');;

      endif;

   }

}

