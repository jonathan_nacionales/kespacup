<?php

class AuthController extends BaseController {

	public function getLogin()
	{
		return View::make('auth.login');
	}
	
	public function postLogin() {
	
		if (Auth::attempt(array('username' => Input::get('username'), 'password' => Input::get('password')), true)):
		
			return Redirect::intended('results');
		
		else:
		
			return Redirect::to('auth/login')->with('message', 'Login Failed');
		
		endif;
	
	}
	
	
	public function getLogout() {
	
		if( Auth::check() ):
			Auth::logout();
			Return Redirect::to('auth/login')->with('message', 'You have been logged out');
		endif;
		Return Redirect::to('/');
	}
	

}
