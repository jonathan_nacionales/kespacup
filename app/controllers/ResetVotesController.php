
<?php

class ResetVotesController extends BaseController {
	

	public function index() {

		return View::make('reset_votes.index', array());
	}

	public function postIndex() {
      Vote::truncate();
      User::truncate();
      return Redirect::to('reset_votes')->with('message_success', 
      'Votes Reset!');;
	}
}

