<?php

use Aws\Common\Exception\MultipartUploadException;
use Aws\S3\Model\MultipartUpload\UploadBuilder;

class VideoController extends BaseController {



	public function index() {
		$videos = Video::all();
		return View::make('video.index', array('videos'=>$videos));
	}



	public function getUpload() {

		return View::make('video.upload');

	}



	public function postUpload() {

		if(Input::file('file')):
			
			$file = Input::file('file');
			$path = $file->getRealPath();
			$ext  = strtolower($file->getClientOriginalExtension());
			$name = $file->getClientOriginalName();
			$name = explode('.',$name)[0];
	 		$size  = $file->getSize();

			if($ext == 'mp4'):
				

				$video = new Video;
				$video->title = $name;
				$video->url = 'blank';
				$video->save();

				$video_id = $video->id;

				$key  = 'uploads/videos/'.$video_id.'-'.md5($name).'.'.$ext;

				$bucket = 'prod-ogn.azubu.tv';
				$keyname = $key;
											

				$s3 = AWS::get('s3');

				$uploader = UploadBuilder::newInstance()
					->setClient($s3)
					->setSource($path)
					->setBucket($bucket)
					->setKey($keyname)
					->setOption('ACL', 'public-read')
					->setOption('ContentType', 'video/mp4')
					->setConcurrency(3)
					->build();
				try {

					$uploader->upload();
					$video = Video::find($video_id);
					$video->url = "http://prod-ogn.azubu.tv.s3.amazonaws.com/".$key;
					$video->save();

					if( Input::get('player') != 'none' AND is_numeric(Input::get('player')) ):
						$p = Player::find(Input::get('player'));
						$p->video_url = "http://prod-ogn.azubu.tv.s3.amazonaws.com/".$key;
						$p->save();
					endif;

					return Redirect::to('videos/upload')->with('message_success', 'Video Uploaded');

				} catch (MultipartUploadException $e) {

					$uploader->abort();

					return Redirect::to('videos/upload')->with('message', 'Upload Failed: '.$e->getMessage() );

				}


			else:
				return Redirect::to('videos/upload')->with('message', 'Upload Failed: Video Must be MP4');
			endif;

		else:
			return Redirect::to('videos/upload')->with('message', 'Upload Failed: Select a video to upload');
		endif;
	}

}
