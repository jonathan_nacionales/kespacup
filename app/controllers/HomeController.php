<?php

class HomeController extends BaseController {


	public function index()
	{
		$vote_payload = null;

		if( Auth::check() ):
			
			$total_votes = Azubu::get_total_votes();
			$vote_payload = $total_votes * 0.1;
			
		endif;
		
		$voting_disabled_for_user = false;
		$voted_for = null;
		$next_vote = null;

		if( Cookie::get('ogn_azubu_mvp_polling_app_24hour_check') ):

			$cookie = Cookie::get('ogn_azubu_mvp_polling_app_24hour_check');
			$array = json_decode($cookie, true);

			$next_vote = $array['next_vote'];
			$voted_for = $array['voted_for'];

			$voting_disabled_for_user = true;

		endif;

		$display_featured_players = false;
		$featured_player_count = Player::where('featured','=',1)->count();
		$featured_players = null;

		if( $featured_player_count > 0 ):

			$featured_players = Player::where('featured','=',1)->get();
			$display_featured_players = true;

		endif;

      $results = DB::select( DB::raw("
         SELECT 
            UPPER(p.team) as team, 
            p.name,
            p.image_url,
            t.image_url as team_image
         FROM players p inner join teams t
            ON p.team = t.name
         WHERE t.enabled = 1
         GROUP BY p.team, p.name
         ORDER BY p.team;
         "));

      $teams = array();
      foreach( $results as $result ) {
         $teamName = $result->team;
         $playerName = $result->name;
         $imageUrl = $result->image_url;
         $team_image = $result->team_image;

         if( empty( $teams[$teamName] ) ) {
            $teams[$teamName] = new stdclass();
            $teams[$teamName]->players = array();
            $teams[$teamName]->image_url = "";
         }

         $teams[$teamName]->players[] = $result;
         $teams[$teamName]->image_url = $team_image;
      }

		return View::make('vote', array(
			'voting_disabled_for_user'  => $voting_disabled_for_user,
			'voted_for'                 => $voted_for,
			'next_vote'                 => str_replace("-","/", Carbon::createFromTimeStamp($next_vote)->tz('utc') ),
			'vote_payload'              => round($vote_payload),
			'display_featured_players'  => $display_featured_players,
			'featured_players'          => $featured_players,
         'teams'                     => $teams

		));
	}

}
