<?php

class VoteController extends BaseController {

	public function postVote()
	{
		if ( Request::ajax() AND Azubu::voting_enabled() ):

			if( App::environment('local') ):
			
				$ip_via_jquery = 'development';
				
			else:
			
				//$ip_via_jquery = md5( Input::get( 'user_ip' ) );
				$ip_via_jquery = Azubu::get_user_ip();
				
			endif;

			$player = Input::get( 'player' );

			if( $ip_via_jquery ===  Azubu::get_user_ip() ):

				$user = User::firstOrCreate( array('ip_hash' => $ip_via_jquery) );
				
				$user_id = $user->id;

				if( Vote::where('user_id','=',$user_id)->count() > 0 ):

					$latest_vote = Vote::where('user_id','=',$user_id)->orderBy('created_at','DESC')->first();

					$latest_vote_time = $latest_vote->created_at;

					$latest_vote_time_carbon = new Carbon( $latest_vote_time );
					
					$tomorrow = $latest_vote_time_carbon->addDay()->timestamp;
					
					$now = Carbon::now()->timestamp;

					if( $now < $tomorrow ):
					
						$success = false;
						
						$message = Lang::get('messages.error_too_soon');
						
					else:
						
						if( Auth::check() ):
							$voted_for = Azubu::add_vote( $user_id , $player, Input::get('payload'), Auth::user()->id );
						else:
							$voted_for = Azubu::add_vote( $user_id , $player );
						endif;
					
						$success = true;
						
						$message = Lang::get('messages.success_message');
						
					endif;
					
				else:
				
					if( Auth::check() ):
					
						$voted_for = Azubu::add_vote( $user_id , $player, Input::get('payload'), Auth::user()->id );
						
					else:
					
						$voted_for = Azubu::add_vote( $user_id , $player );
						
					endif;
					
					$success = true;
					
					$message = Lang::get('messages.success_message');
					
				endif;
				
			else:
			
				$success = false;
				
				$message = Lang::get('messages.error_system');
				
			endif;

			if( $success AND !App::environment('local') ):
			
				$cookie_array = array(
					'ip_hash' => $ip_via_jquery,
					'next_vote' => Carbon::now()->addDay()->timestamp,
					'voted_for' => $voted_for
				);
				
				$cookie_json = json_encode($cookie_array, true);
				
				Cookie::queue('ogn_azubu_mvp_polling_app_24hour_check', $cookie_json, 1440);
				
			endif;

			return Response::json(array(
				'success' => $success,
				'message' => $message
			));

		else:

			if( ! Request::ajax() ):
				return Response::json(array(
					'success' => false,
					'message' => 'Invalid Request'
				));
			endif;

			if( ! Azubu::voting_enabled() ):
				return Response::json(array(
					'success' => false,
					'message' => 'Voting Disabled'
				));
			endif;
		endif;
	}



	public function getResults() {

		/*
		if( ! isset($_GET[md5( 'azooboo' )]) ):
			die('Not Authorized');
		endif; */

		$total_voters = User::all()->count();
		$total_votes = Vote::all()->count();


		$votes = Vote::all();
		$votes_array = array();

		foreach( $votes as $vote ):

			array_push( $votes_array , $vote->vote );

		endforeach;

		$counts = array_count_values($votes_array);
		arsort( $counts );


		if( isset($_GET['format']) AND $_GET['format'] === 'json' ):

			$json = array(
				'total_voters' => $total_voters,
				'total_votes'  => $total_votes,
				'votes' => $counts
			);

			return Response::json($json);

		endif;


		return View::make('results', array( 'counts' => $counts , 'total_voters' => $total_voters , 'total_votes' => $total_votes ));


	}


	public function getResultsExport() {

		$total_votes = Vote::all()->count();
		$votes = Vote::all();
		$votes_array = array();

		foreach( $votes as $vote ):

			array_push( $votes_array , $vote->vote );

		endforeach;

		$counts = array_count_values($votes_array);
		arsort( $counts );


	}

}
