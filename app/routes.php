<?php

Route::group(array('prefix' => Localize::setLocale()), function()
{
	
	Route::get( '/' , array('uses' => 'HomeController@index') );

});

Route::get( 'auth/login' ,     array('uses' => 'AuthController@getLogin'));
Route::post('auth/login' ,     array('uses' => 'AuthController@postLogin'));
Route::get( 'auth/logout' ,    array('uses' => 'AuthController@getLogout'));
Route::get( 'results' ,        array('uses' => 'VoteController@getResults'));
Route::post('ajax/vote',       array('uses' => 'VoteController@postVote'));

Route::get('admin', function()
{
    return Redirect::to('auth/login');
});

Route::group(array('before' => 'auth'), function(){

	Route::get( 'players' ,           array( 'uses' => 'PlayerController@index' ));
	Route::get( 'players/create' ,    array( 'uses' => 'PlayerController@getCreate' ));
	Route::post('players/create' ,    array( 'uses' => 'PlayerController@postCreate' ));
	Route::get( 'players/{id}/edit' , array( 'uses' => 'PlayerController@getEdit' ));
	Route::post('players/{id}/edit' , array( 'uses' => 'PlayerController@postEdit' ));
	Route::post('players/feature' ,   array( 'uses' => 'PlayerController@postFeature' ));

   Route::get( 'reset_votes', array( 'uses' => 'ResetVotesController@index' ));
   Route::post( 'reset_votes', array( 'uses' => 'ResetVotesController@postIndex' ));

   Route::get( 'teams', array( 'uses' => 'TeamController@index' ));
   Route::get( 'teams/create', array( 'uses' => 'TeamController@getCreate' ));
   Route::post( 'teams/create', array( 'uses' => 'TeamController@postCreate' ));
   Route::get( 'teams/{id}/edit', array( 'uses' => 'TeamController@getEdit' ));
   Route::post( 'teams/{id}/edit', array( 'uses' => 'TeamController@postEdit' ));
/*
   Route::get( 'images', array( 'uses' => 'ImageController@index' ));
*/

	Route::get( 'videos',             array('uses' => 'VideoController@index'));
	Route::get( 'videos/upload',      array('uses' => 'VideoController@getUpload'));
	Route::post('videos/upload',      array('uses' => 'VideoController@postUpload'));

	Route::get( 'settings',   array('uses' => 'SettingsController@index'));
	Route::post('settings',   array('uses' => 'SettingsController@postSettings'));

});
