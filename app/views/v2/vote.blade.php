<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Azubu | OnGameNet</title>
    <link rel="shortcut icon" href="http://prod-content.azubu.tv.s3.amazonaws.com/wp-content/uploads/2014/07/azubu_favicon.png"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,800" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="http://prod-ogn.azubu.tv.s3.amazonaws.com/css/app.css" rel="stylesheet">


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .countdown {
            font-family: 'Open Sans', sans-serif;
            font-weight: 300;
            color: #fff;
            padding: 10px;
            font-size: 18px;

        }

        .countdown #timer {
            font-size: 50px;
        }

        #lang-selector {
            position: fixed;
            top: 13px; right: 13px;
            z-index: 14
        }

        .video {
            position: relative;
            padding-bottom: 56.25%; /* 16:9 */
            padding-top: 25px;
            height: 0;
        }
        .video iframe {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }

        body {
            margin-top: 100px;
        }

        .img-center { margin: 0 auto; }
        .about p {
            text-align: center;
        }

        .row-gap { margin-top: 40px; }
        .vs-bottom {
            border-top: 1px solid rgba(255,255,255,0.3);
            padding-top: 25px;
        }

        #current-match {
            padding: 15px 0;
            color: #fff;
            font-size: 30px;
            font-weight: 100;
        }


        .stats {
            position: relative;
        }

        .stat-box-container {
            display: none;
            position: absolute;
            top: 0; left: 0; right: 0; bottom: 0;
        }

        .stat-box-bg {
            position: absolute;
            top: 0; left: 0; right: 0; bottom: 0;
            background: #000;
            opacity: 0.7;
            z-index: 10;
        }

        .stat-box-overlay {
            position: absolute;
            top: 0; left: 0; right: 0; bottom: 0;
            color: #fff;
            padding: 20px;
            z-index: 11;
        }

        .stat-box-close {
            position: absolute;
            top: 10px; right: 10px;
            color: #fff;
            z-index: 12;
        }
        .stat-box-close:hover {
            color: #fff;
            opacity: 0.5;
            cursor: pointer;
        }

    </style>



    @include('ga')

</head>
<body>

<div id="success_message">
    <div class="container">
        <h1><i class="fa fa-check"></i> {{ Lang::get('messages.success')  }}</h1>
        <span id="success_text"></span>
    </div>
</div>

<div id="error_message">
    <div class="container">
        <h1><i class="fa fa-times"></i> {{ Lang::get('messages.error')  }}</h1>
        <span id="error_text"></span>
    </div>
</div>

<div id="lang-selector">
    <a href="/ko" class="btn btn-default btn-xs">KO</a>
    <a href="/en" class="btn btn-default btn-xs">EN</a>
</div>


<div id="azubu-nav">
    <div class="container">
        <a href="http://azubu.tv">
            <img src="http://img.azubu.tv/bundles/azubuweb/img/azubu_blue.png" width="100"/>
        </a>
    </div>
</div>

<div class="container-fluid" id="main">

    <div class="row">
        <div class="col-md-3 about">
            <p>
                <img src="{{ asset('img/superplay_logo.png') }}" class="img-responsive img-center" />
            </p>
            <p>
                {{ Lang::get('messages.welcome') }}
            </p>
            <p>
                {{ Lang::get('messages.briefing') }}
            </p>
            <p>
                - {{ Lang::get('messages.point_1') }}<br />
                - {{ Lang::get('messages.point_2') }}<br />
                - {{ Lang::get('messages.point_3') }}
            </p>
        </div>
        <div class="col-md-6">
            <div class="video">
                <iframe src="http://www.azubu.tv/azubulink/embed=ongamenet" width="100%" height="100%"></iframe>
            </div>
        </div>
        <div class="col-md-3 about countdown">
            <p>
                VOTING ENDS: <br />
                <span id="timer">00:00:00</span>
            </p>
        </div>
    </div>

    <div class="row row-gap">
        <div class="col-md-3 stats">
            <div class="stat-box-container left">
                <a class="stat-box-close"><i class="fa fa-times"></i></a>
                <div class="stat-box-overlay">STATS</div>
                <div class="stat-box-bg"></div>
            </div>
            <img src="{{ asset('img/team_logos/cj_entus_big.png') }}" class="img-responsive img-center" />
        </div>
        <div class="col-md-6">

            <div id="current-match">

                CURRENT MATCH UP

            </div>

            <div class="row">

                <div class="col-md-2 col-md-offset-1"><a href="" class="show-stats" data-side="left"><img src="{{ asset('img/players/cj/ambition.jpg') }}" class="img-responsive img-center" /></a></div>
                <div class="col-md-2"><a href="" class="show-stats" data-side="left"><img src="{{ asset('img/players/cj/ambition.jpg') }}" class="img-responsive img-center" /></a></div>
                <div class="col-md-2"><a href="" class="show-stats" data-side="left"><img src="{{ asset('img/players/cj/ambition.jpg') }}" class="img-responsive img-center" /></a></div>
                <div class="col-md-2"><a href="" class="show-stats" data-side="left"><img src="{{ asset('img/players/cj/ambition.jpg') }}" class="img-responsive img-center" /></a></div>
                <div class="col-md-2"><a href="" class="show-stats" data-side="left"><img src="{{ asset('img/players/cj/ambition.jpg') }}" class="img-responsive img-center" /></a></div>

            </div>

            <div class="row vs-bottom">

                <div class="col-md-2 col-md-offset-1"><a href="" class="show-stats" data-side="right"><img src="{{ asset('img/players/cj/ambition.jpg') }}" class="img-responsive img-center" /></a></div>
                <div class="col-md-2"><a href="" class="show-stats" data-side="right"><img src="{{ asset('img/players/cj/ambition.jpg') }}" class="img-responsive img-center" /></a></div>
                <div class="col-md-2"><a href="" class="show-stats" data-side="right"><img src="{{ asset('img/players/cj/ambition.jpg') }}" class="img-responsive img-center" /></a></div>
                <div class="col-md-2"><a href="" class="show-stats" data-side="right"><img src="{{ asset('img/players/cj/ambition.jpg') }}" class="img-responsive img-center" /></a></div>
                <div class="col-md-2"><a href="" class="show-stats" data-side="right"><img src="{{ asset('img/players/cj/ambition.jpg') }}" class="img-responsive img-center" /></a></div>

            </div>
        </div>
        <div class="col-md-3">
            <div class="stat-box-container right">
                <a class="stat-box-close"><i class="fa fa-times"></i></a>
                <div class="stat-box-overlay">STATS</div>
                <div class="stat-box-bg"></div>
            </div>
            <img src="{{ asset('img/team_logos/cj_entus_big.png') }}" class="img-responsive img-center" />
        </div>
    </div>

</div>








<script>
    var user_ip = null;
    var root_url = '{{ URL::to('/') }}/';
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

<script src="http://l2.io/ip.js?var=user_ip"></script>
<script src="http://prod-ogn.azubu.tv.s3.amazonaws.com/js/app.js"></script>


<script>

    $('.show-stats').click(function( event ){

        var $this = $(this);
        var side = $this.data('side');
        var statContainer = $('.stat-box-container.'+side);

        if( ! statContainer.hasClass('active') ){
            statContainer.addClass('active');
            statContainer.fadeIn();
        }

        event.preventDefault();
    });


    $('.stat-box-close').click(function( event ){

        $(this).closest('.stat-box-container').fadeOut();
        $(this).closest('.stat-box-container').removeClass('active');


        event.preventDefault();
    });




</script>

</body>
</html>