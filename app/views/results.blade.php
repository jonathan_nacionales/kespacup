<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Azubu | KeSPA Cup</title>
    <link rel="shortcut icon" href="http://prod-content.azubu.tv.s3.amazonaws.com/wp-content/uploads/2014/07/azubu_favicon.png"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,800" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="http://prod-ogn.azubu.tv.s3.amazonaws.com/css/app.css" rel="stylesheet">


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>

        body { margin-top: 80px; }
        .meta {
            text-align: left;
            color: #fff;
            font-size: 18px;
            font-family: 'Open Sans', sans-serif;
            font-weight: 300;
        }
        .meta span {
            font-size: 36px;
            margin: 0 20px;
            font-weight: 300;
        }

        h1 {
            font-size: 30px;
            font-family: 'Open Sans', sans-serif;
            font-weight: 300;
            color: #fff;
            margin-bottom: 30px;
        }
        .btn-default {
            background: #000;
            border-color: #111;
            color: #CCCCCC;
        }
        .btn-default:hover {
            background: #111;
            border-color: #111;
            color: #CCCCCC;
        }

        .btn-default.azubu {
            background: #009fd8;
            border-color: #111;
            color: #fff;
        }
        .btn-default.azubu:hover {
            background: #038bbc;
            border-color: #111;
            color: #fff;
        }
    </style>

</head>
<body>

    <div id="azubu-nav">
        <div class="container">
            <a href="http://azubu.tv"
               class="btn btn-default text-uppercase hidden-sm hidden-xs"
               style="margin-top:2px;">
               <img src="{{ asset('img/Azubu-Logo-Site-02.png') }}" 
                  width="200" /> 
            </a>
            <a href="http://azubu.tv/spotvgames_global" class="btn btn-default pull-right text-uppercase" style="margin-top: 2px;">
                Back to
               <img src="http://prod-content.azubu.tv.s3.amazonaws.com/wp-content/uploads/2014/07/azubu_favicon.png" />
               SPOTVGAMES_GLOBAL
            </a>
        </div>
    </div>


    <div class="container">
		@if( Auth::check() )
			@include('partials.adminmenu')
		@endif
        <a href="{{ URL::to('/') }}" class="btn btn-default azubu"><i class="fa fa-arrow-circle-left"></i> <strong>VOTING PAGE</strong></a>

        <h1>RESULTS</h1>


        @foreach( $counts as $player => $player_votes )
        <?php
            $percentage = round( $player_votes / $total_votes * 100 , 2 );

                if( $player == 'Cap Jack' ):
                    $player = 'Cpt Jack';
                endif;

            ?>



                <div class="meta">
                    <span>{{ $percentage  }}%</span> <strong class="text-uppercase">{{ $player  }}</strong>
                </div>
                <div class="col-md-12">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentage }}%;"> </div>
                    </div>
                </div>




        @endforeach

    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>
