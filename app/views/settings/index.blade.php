<!DOCTYPE html>
<html>

	<head>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
	</head>
	
	<body>
		
		<div class="container">
			@include('partials.adminmenu')

			<h1>SETTINGS</h1>
			
			@if( Session::get('message') )
				<div class="alert alert-danger" role="alert">{{ Session::get('message') }}</div>
			@endif

			@if( Session::get('message_success') )
				<div class="alert alert-success" role="alert">{{ Session::get('message_success') }}</div>
			@endif
			
			
			{{ Form::open() }}
				Voting Enabled?
				<select id="voting_enabled" name="voting_enabled" class="form-control">
					<option value="true" @if( Azubu::voting_enabled() ) selected="selected" @endif >ENABLED</option>
					<option value="false" @if( !Azubu::voting_enabled() ) selected="selected" @endif>DISABLED</option>
				</select>

				<br />
				<button class="btn btn-primary">SAVE</button>
					
			{{ Form::close() }}
			
		</div>
	
	</body>


</html>