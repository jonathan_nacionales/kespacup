<div class="panel panel-default" style="margin-top: 10px;">
	<div class="panel-body" style="text-align: left;">

		<span class="label label-primary" style="margin-right: 10px;">
			{{ Auth::user()->username }}
		</span>

		@if( Azubu::voting_enabled() )
			<span class="label label-success">
				VOTING ENABLED
			</span>

		@else
			<span class="label label-danger">
				VOTING DISABLED
			</span>
		@endif

		<div class="pull-right">
			<a href="{{ URL::to('/') }}" class="btn btn-primary btn-xs">HOME</a>
			<a href="{{ URL::to('settings') }}" class="btn btn-primary btn-xs">SETTINGS</a>
			<a href="{{ URL::to('players') }}" class="btn btn-primary btn-xs">PLAYERS</a>
			<a href="{{ URL::to('teams') }}" class="btn btn-primary btn-xs">TEAMS</a>
			<a href="{{ URL::to('videos') }}" class="btn btn-primary btn-xs">VIDEOS</a>
			<a href="{{ URL::to('videos/upload') }}" class="btn btn-primary btn-xs">UPLOAD</a>
			<a href="{{ URL::to('results') }}" class="btn btn-primary btn-xs">RESULTS</a>
			<a href="{{ URL::to('reset_votes') }}" class="btn btn-primary btn-xs">RESET VOTES</a>
			<a href="{{ URL::to('auth/logout') }}" class="btn btn-primary btn-xs">LOGOUT</a>
		</div>
	</div>
</div>	
