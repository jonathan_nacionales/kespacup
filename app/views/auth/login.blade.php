<!DOCTYPE html>
<html>

	<head>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
	</head>
	
	<body>
		
		<div class="container">
			<h1>LOGIN</h1>
			
			@if( Session::get('message') )
				<div class="alert alert-danger" role="alert">{{ Session::get('message') }}</div>
			@endif
			
			@if( Auth::check() )
			
				<div class="alert alert-danger" role="alert">
					Logged in as: {{ Auth::user()->username }}. <a href="{{ URL::to('auth/logout') }}">Logout</a><br />
					Go back <a href="{{ URL::to('/') }}">Home</a>
				</div>
				
			@else
			
				{{ Form::open() }}
					
					<input type="text" name="username" id="username" placeholder="Username" class="form-control" /><br />
					<input type="password" name="password" id="password" placeholder="Password" class="form-control" /><br />
					<button class="btn btn-primary">LOGIN</button>
					
				{{ Form::close() }}
			@endif
		</div>
	
	</body>


</html>