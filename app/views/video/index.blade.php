<!DOCTYPE html>
<html>

	<head>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
	</head>
	
	<body>
		
		<div class="container">
			@include('partials.adminmenu')		
			<h1>VIDEOS</h1>
			
			@if( Session::get('message') )
				<div class="alert alert-danger" role="alert">{{ Session::get('message') }}</div>
			@endif

			@if( Session::get('message_success') )
				<div class="alert alert-success" role="alert">{{ Session::get('message_success') }}</div>
			@endif
			


			<table class="table table-hover">
				<thead>
					<tr>
						<th>ID</th>
						<th>NAME</th>
						<th>URL</th>
					</tr>

				</thead>

				<tbody>

					@foreach( $videos as $video )

					<tr>

						<td>{{ $video->id }}</td>

						<td>{{ $video->title }}</td>
						
						<td>{{ $video->url }}</td>
					
					</tr>

					@endforeach

				</tbody>


			</table>

			
		</div>
	</body>


</html>