<!DOCTYPE html>
<html>

	<head>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
	</head>
	
	<body>
		
		<div class="container">
			@include('partials.adminmenu')

			<h1>UPLOAD VIDEO</h1>
			
			@if( Session::get('message') )
				<div class="alert alert-danger" role="alert">{{ Session::get('message') }}</div>
			@endif

			@if( Session::get('message_success') )
				<div class="alert alert-success" role="alert">{{ Session::get('message_success') }}</div>
			@endif
			
			
			{{ Form::open(array('files' => true, 'class' => 'dropzone', 'id' => 'video-uploader')) }}
				
				
				<p>MUST BE .MP4 FORMAT</p>	
				
            <!--
				<input type="file" name="file" id="file" /><br />
            -->
            {{Form::file('file')}}
            

				Attach video to which player?
				<select name="player" class="form-control">
						<option value="none">None</option>
					@foreach( Player::all() as $player )

						<option value="{{ $player->id }}">{{ $player->name }}</option>

					@endforeach
				</select>

				<hr />

				<button class="btn btn-primary">UPLOAD</button>
					
			{{ Form::close() }}
			
		</div>
	



	</body>


</html>
