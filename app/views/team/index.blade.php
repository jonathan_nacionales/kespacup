<!DOCTYPE html>
<html>

	<head>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
	</head>
	
	<body>
		
		<div class="container">
			@include('partials.adminmenu')		
			<h1>TEAMS</h1>
			
			@if( Session::get('message') )
				<div class="alert alert-danger" role="alert">{{ Session::get('message') }}</div>
			@endif

			@if( Session::get('message_success') )
				<div class="alert alert-success" role="alert">{{ Session::get('message_success') }}</div>
			@endif
			
         <div>
            <span><a href="/teams/create">create a team</a></span>
         </div>

			<table class="table table-hover">
				<thead>
					<tr>
						<th>ID</th>
						<th>NAME</th>
						<th>HAS IMAGE?</th>
						<th>Enabled?</th>
						<th></th>
					</tr>

				</thead>

				<tbody>

					@foreach( $teams as $team )

					<tr>

						<td>{{ $team->id }}</td>
						<td>{{ $team->name }}</td>
						<td>{{ ($team->image_url ? 'YES' : 'NO') }}</td>
						<td>{{ ($team->enabled ? 'YES' : 'NO') }}</td>
						<td><a href="{{ URL::to('teams/'.$team->id.'/edit') }}">EDIT</a></td>
					
					</tr>

					@endforeach

				</tbody>


			</table>

			
		</div>
	
	</body>


</html>
