<!DOCTYPE html>
<html>

	<head>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
	</head>
	
	<body>
		
		<div class="container">
			@include('partials.adminmenu')

			<h1>{{ $team->name }}</h1>
			
			@if( Session::get('message') )
				<div class="alert alert-danger" role="alert">{{ Session::get('message') }}</div>
			@endif

			@if( Session::get('message_success') )
				<div class="alert alert-success" role="alert">{{ Session::get('message_success') }}</div>
			@endif
			
			
			{{ Form::open() }}
					
				<input type="text" name="name" id="name" placeholder="Name" class="form-control" value="{{ $team->name }}" /><br />

				IMAGE URL<br />
				<input type="text" name="image_url" id="image_url"
            placeholder="Image URL" class="form-control" value="{{ $team->image_url }}" /><br />

				ENABLED?<br />
				<select id="enabled" name="enabled" class="form-control">
					<option {{ ($team->enabled == 0 ? 'selected="selected"' : '') }} value="0">NO</option>
					<option {{ ($team->enabled == 1 ? 'selected="selected"' : '') }} value="1">YES</option>
				</select>
				<br />


				<button class="btn btn-primary">SAVE</button>
					
			{{ Form::close() }}
			
		</div>
	
	</body>


</html>
