<!DOCTYPE html>
<html>

	<head>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
	</head>
	
	<body>
		
		<div class="container">
			@include('partials.adminmenu')		
			<h1>PLAYERS</h1>
			
			@if( Session::get('message') )
				<div class="alert alert-danger" role="alert">{{ Session::get('message') }}</div>
			@endif

			@if( Session::get('message_success') )
				<div class="alert alert-success" role="alert">{{ Session::get('message_success') }}</div>
			@endif
			
         <div>
            <span><a href="/players/create">create a player</a></span>
         </div>


			<table class="table table-hover">
				<thead>
					<tr>
						<th>ID</th>
						<th></th>
						<th>NAME</th>
						<th>TEAM</th>
						<th>HAS IMAGE?</th>
						<th>HAS VIDEO?</th>
						<th></th>
					</tr>

				</thead>

				<tbody>

					@foreach( $players as $player )

					<tr>

						<td>{{ $player->id }}</td>
						<td>
							<button class="btn btn-xs btn-{{ ($player->featured == 1 ? 'success' : 'default') }} feature" data-player="{{ $player->id }}" data-status="{{ $player->featured }}">
								{{ ($player->featured == 1 ? 'FEATURED' : 'FEATURE') }}
							</button>
						</td>
						<td>{{ $player->name }}</td>
						<td>{{ $player->team }}</td>
						<td>{{ ($player->image_url ? 'YES' : 'NO') }}</td>
						<td>{{ ($player->video_url ? 'YES' : 'NO') }}</td>
						<td><a href="{{ URL::to('players/'.$player->id.'/edit') }}">EDIT</a></td>
					
					</tr>

					@endforeach

				</tbody>


			</table>

			
		</div>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script>

		(function($){

			var featureButton = $('button.feature');

			featureButton.click(function(event){

				var $this = $(this);
				var player_id = $this.data('player');
				var status =    $this.data('status');


				$this.attr('disabled','disabled');

				var data = {
					'player_id' : player_id,
					'status' : status
				};

				$.post('{{ URL::to('players/feature') }}', data, function( response ){

						if( response.success ) {

							if( status == 0 ) {

								$this.removeClass('btn-default');
								$this.addClass('btn-success');
								$this.data('status', 1);
								$this.html('FEATURED');

							}

							if( status == 1 ) {

								$this.addClass('btn-default');
								$this.removeClass('btn-success');
								$this.data('status', 0);
								$this.html('FEATURE');

							}

							$this.removeAttr('disabled');
						}
						else {
							alert('ERROR: please refresh and try again');
						}


				});

				event.preventDefault();
			});



		})( jQuery );


		</script>

	
	</body>


</html>
