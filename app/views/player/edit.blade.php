<!DOCTYPE html>
<html>

	<head>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
	</head>
	
	<body>
		
		<div class="container">
			@include('partials.adminmenu')

			<h1>{{ $player->name }}</h1>
			
			@if( Session::get('message') )
				<div class="alert alert-danger" role="alert">{{ Session::get('message') }}</div>
			@endif

			@if( Session::get('message_success') )
				<div class="alert alert-success" role="alert">{{ Session::get('message_success') }}</div>
			@endif
			
			
			{{ Form::open() }}
					
				<input type="text" name="name" id="name" placeholder="Name" class="form-control" value="{{ $player->name }}" /><br />

				IMAGE URL<br />
				<input type="text" name="image_url" id="image_url" placeholder="Image URL" class="form-control" value="{{ $player->image_url }}" /><br />

				VIDEO URL <br />
				<input type="text" name="video_url" id="video_url" placeholder="Video URL" class="form-control" value="{{ $player->video_url }}" /><br />

				FEATURED?<br />
				<select id="featured" name="featured" class="form-control">
					<option {{ ($player->featured == 0 ? 'selected="selected"' : '') }} value="0">NO</option>
					<option {{ ($player->featured == 1 ? 'selected="selected"' : '') }} value="1">YES</option>
				</select>
				<br />

				TEAM<br />
            <!--
				<input type="text" name="team" id="team" class="form-control" value="{{ $player->team }}" /><br />
            -->
				<select id="team" name="team" class="form-control">
               @foreach( $teams as $team )
               <option {{$team->selected == 'selected' ? 'selected="selected"' : ''}} value="{{ $team->value }}">{{ $team->name }}</option>
               @endforeach
            </select>

            <!--
				<select id="team" name="team" class="form-control">
					<option {{ ($player->team == 'cj entus' ? 'selected="selected"' : '') }} value="cj entus">CJ ENTUS</option>
					<option {{ ($player->team == 'ge tigers' ? 'selected="selected"' : '') }} value="ge tigers">GE TIGERS</option>
					<option {{ ($player->team == 'incredible miracle' ? 'selected="selected"' : '') }} value="incredible miracle">INCREDIBLE MIRACLE</option>
					<option {{ ($player->team == 'jin air' ? 'selected="selected"' : '') }} value="jin air">JIN AIR</option>
					<option {{ ($player->team == 'kt rolster' ? 'selected="selected"' : '') }} value="kt rolster">KT ROLSTER</option>
					<option {{ ($player->team == 'najin e-mfire' ? 'selected="selected"' : '') }} value="najin e-mfire">NAJIN E-MFIRE</option>
					<option {{ ($player->team =='samsung galaxy' ? 'selected="selected"' : '') }} value="samsung galaxy">SAMSUNG GALAXY</option>
					<option {{ ($player->team == 'sk telecom t1' ? 'selected="selected"' : '') }} value="sk telecom t1">SK TELECOM T1</option>
				</select>
            -->

				<br />
				<button class="btn btn-primary">SAVE</button>
					
			{{ Form::close() }}


			<hr />
			<div class="row">

				<div class="col-lg-6">
					@if( $player->image_url )
						<img src="{{ $player->image_url }}" class="img-responsive" />
					@endif
				</div>
				<div class="col-lg-6"></div>

			</div>

			<hr />
			
		</div>
	
	</body>


</html>
