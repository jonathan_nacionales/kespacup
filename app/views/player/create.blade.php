<!DOCTYPE html>
<html>

	<head>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
	</head>
	
	<body>
		
		<div class="container">
			@include('partials.adminmenu')

			<h1>CREATE PLAYER</h1>
			
			@if( Session::get('message') )
				<div class="alert alert-danger" role="alert">{{ Session::get('message') }}</div>
			@endif

			@if( Session::get('message_success') )
				<div class="alert alert-success" role="alert">{{ Session::get('message_success') }}</div>
			@endif
			
			
			{{ Form::open() }}
					
				<input type="text" name="name" id="name" placeholder="Name" class="form-control" /><br />
            <!--
				<input type="text" name="team" id="team" placeholder="Team"class="form-control"  /><br />
            -->

				IMAGE URL<br />
				<input type="text" name="image_url" id="image_url" placeholder="Image URL" class="form-control" /><br />

				TEAM<br />
				<select id="team" name="team" class="form-control">
               @foreach( $teams as $team )
               <option value="{{ $team->value }}">{{ $team->name }}</option>
               @endforeach
            </select>
            <!--
				<select id="team" name="team" class="form-control">
					<option value="" selected="selected">PLEASE SELECT</option>
					<option value="cj entus">CJ ENTUS</option>
					<option value="ge tigers">GE TIGERS</option>
					<option value="incredible miracle">INCREDIBLE MIRACLE</option>
					<option value="jin air">JIN AIR</option>
					<option value="kt rolster">KT ROLSTER</option>
					<option value="najin e-mfire">NAJIN E-MFIRE</option>
					<option value="samsung galaxy">SAMSUNG GALAXY</option>
					<option value="sk telecom t1">SK TELECOM T1</option>
				</select>
            -->

				<br />
				<button class="btn btn-primary">CREATE</button>
					
			{{ Form::close() }}
			
		</div>
	
	</body>


</html>
