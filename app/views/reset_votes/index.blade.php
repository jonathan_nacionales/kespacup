<!DOCTYPE html>
<html>

	<head>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
	</head>
	
	<body>
		
		<div class="container">
			@include('partials.adminmenu')

			<h1>RESET VOTES</h1>

			@if( Session::get('message_success') )
				<div class="alert alert-success" role="alert">{{ Session::get('message_success') }}</div>
			@endif
			
			{{ Form::open() }}
					
				<br />
				<button class="btn btn-primary">RESET VOTES</button>
					
			{{ Form::close() }}
			
		</div>
	
	</body>


</html>
