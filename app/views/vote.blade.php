<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Azubu | KeSPA Cup</title>
    <link rel="shortcut icon" href="http://prod-content.azubu.tv.s3.amazonaws.com/wp-content/uploads/2014/07/azubu_favicon.png"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,800" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="//vjs.zencdn.net/4.11/video-js.css" rel="stylesheet">
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">


	<script src="//vjs.zencdn.net/4.11/video.js"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        body {
            background: url('{{ asset('img/ogn_background.jpg')  }}') no-repeat center center fixed;
        }
    </style>

    @include('ga')

</head>
<body>

<a class="fullscreen-player-close-button"><i class="fa fa-minus-square"></i></a>
<div class="fullscreen-player"></div>
<div class="fullscreen-player-inner">
	<div class="fullscreen-player-video">
		<video id="featured-video" 
         class="video-js vjs-default-skin" 
         controls preload="none" 
         data-setup="{}" 
         height="auto" 
         width="auto">
				<source id="featured-video-source" 
               src="http://players.brightcove.net/3361910549001/13b53caa-9aa1-48be-a9f2-657520f437af_default/index.html?playlistId=ref:spotvgames_globalPlaylist&channel=spotvgames_global" 
               type='video/mp4' />
				<p class="vjs-no-js">
               To view this video please enable JavaScript, and consider 
               upgrading to a web browser that 
               <a href="http://videojs.com/html5-video-support/" 
                  target="_blank">supports HTML5 video</a>
            </p>
		</video>
	</div>
</div>



<div id="success_message">
    <div class="container">
        <h1><i class="fa fa-check"></i> {{ Lang::get('messages.success')  }}</h1>
        <span id="success_text"></span>
    </div>
</div>

<div id="error_message">
    <div class="container">
        <h1><i class="fa fa-times"></i> {{ Lang::get('messages.error')  }}</h1>
        <span id="error_text"></span>
    </div>
</div>

<div id="lang-selector">
    <a href="/ko" class="btn btn-default btn-xs">KO</a>
    <a href="/en" class="btn btn-default btn-xs">EN</a>
</div>


<div id="azubu-nav">
    <div class="container">
        <a href="http://azubu.tv" 
class="btn btn-default text-uppercase hidden-sm hidden-xs"
         style="margin-top:2px;">
            <img src="{{ asset('img/Azubu-Logo-Site-02.png') }}" 
            width="200" /> 
            <!--
            <img src="http://img.azubu.tv/bundles/azubuweb/img/azubu_blue.png" width="100"/>
            -->
        </a>

         @if( Localize::getCurrentLocale() == 'en')
           <a href="http://azubu.tv/spotvgames_global" class="btn btn-default pull-right text-uppercase hidden-sm hidden-xs" style="margin-top: 2px;">
               Back to
               <img src="http://prod-content.azubu.tv.s3.amazonaws.com/wp-content/uploads/2014/07/azubu_favicon.png" />
               SPOTVGAMES_GLOBAL
           </a>
         @else
           <a href="http://azubu.tv/spotvgames" class="btn btn-default pull-right text-uppercase hidden-sm hidden-xs" style="margin-top: 2px;">
               Back to
               <img src="http://prod-content.azubu.tv.s3.amazonaws.com/wp-content/uploads/2014/07/azubu_favicon.png" />
               SPOTVGAMES
           </a>
         @endif
    </div>
</div>

<div class="container">

    <div class="row" style="margin-top: 50px;">
		@if( Auth::check() )
			@include('partials.adminmenu')
		@endif



        <div class="col-md-4 about">
            <p>
                <img src="{{ asset('img/Azubu-MVP-master-RGB-300dpi_Logo_White.png') }}" 
                class="img-responsive img-center" />
            </p>
            	{{ Lang::get('messages.point') }}<br />

        </div>
        <div class="col-md-8 video-container">
            <div class="video" id="stream">

            	@if( Localize::getCurrentLocale() == 'en')
            		<iframe src="http://embed.azubu.tv/spotvgames" width="100%" height="100%" frameborder="0" allowfullscreen></iframe>
            	@else
            		<iframe src="http://embed.azubu.tv/spotvgames_global" width="100%" height="100%" frameborder="0" allowfullscreen></iframe>
            	@endif


            </div>
        </div>
    </div>

        <div class="row">

        	<div class="col-md-4">
         <!--
                <a href="{{ URL::to('results') }}" class="btn btn-default btn-block azubu" style="margin-top: 2px;">
                    VIEW <strong>RESULTS</strong>
                </a>
         -->
        	</div>

        	<div class="col-md-8">
	            <p style="font-size: 12px; color: #fff;">
	            </p>
        	</div>

        </div>


	@if($display_featured_players)

         <div class="voting-wrapper">
         <div class="voting-wrapper-inner">
	<div class="featured-players">
      <!--
		<h1>{{ Lang::get('messages.candidates') }}</h1>
      -->
		<h1>{{ Lang::get('messages.vote_message') }}</h1>

		<div class="row">

			@foreach( $featured_players as $player )
				<div class="col-md-3 col-sm-6 col-xs-6">
					<h3>{{ $player->name }}</h3>

					<div class="image-holder">
						<img src="{{ $player->image_url }}" class="img-responsive" />
						<div class="controls">
							<div class="row">
								<div class="col-md-12">
									<button class="btn btn-success btn-block vote" data-player="{{ $player->name }}">{{ Lang::get('messages.vote') }}</button>
								</div>
                        <!--
								<div class="col-md-12">
									<button class="btn btn-primary btn-block azubu featured-player-play-button" data-video="{{ $player->video_url }}"><i class="fa fa-play"></i></button>
								</div>
                        -->
							</div>
						</div>
						<div class="background"></div>
					</div>

				</div>
			@endforeach
		</div>
	</div>
   </div>
   </div>
	@else
   <div class="row">
      <h1 style="color: white"> Voting will commence Saturday November 14 at
      18:30 KST (GMT +9) </h1>
   </div>
	@endif

</div>

<script>
	var payload = 1;
    var user_ip = null;
    var root_url = '{{ URL::to('/') }}/';
	@if( Auth::check() )
	payload = {{ $vote_payload }};
	@endif
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<!-- <script src="http://l2.io/ip.js?var=user_ip"></script> -->
<script src="{{ asset('js/app.js') }}"></script>



<!-- <script src="http://prod-ogn.azubu.tv.s3.amazonaws.com/js/2015-01-21/app.js"></script> -->


@if( !Azubu::voting_enabled() )
    <script>
        $(document).ready(function(){
           	$('.vote').attr('disabled' , 'disabled');
        });

    </script>
@endif

@if( $voting_disabled_for_user )
    <script src="js/vendor/jquery.countdown.min.js"></script>
    <script>
        $(document).ready(function(){
            var next_vote = "{{ $next_vote  }}";
            var voted_for = "{{ $voted_for  }}";
            var voted_for_button = $('[data-player='+ voted_for +']');
            voted_for_button.html("<i class=\"fa fa-check\"></i>");
            voted_for_button.addClass('btn-success');
            $('.vote').attr('disabled' , 'disabled');
        });

    </script>
@endif


</body>
</html>
