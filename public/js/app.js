$(function(){

	var $votingIsEnabled = true;

	/*
		Register user's votes
	*/
    var voteButton = $('.vote');
    voteButton.click(function( e ){

		if( $votingIsEnabled ) {
		
			var $this = $(this);
			var $selectedPlayer = $this.data('player');

			putButtonInLoadingState( $this );

			var $data = {
				'user_ip' : user_ip,
				'player'  : $selectedPlayer,
				'payload' : payload
			};

			
			$.post( root_url + 'index.php/ajax/vote' , $data , function( response ){
				if( response.success ) {
					putButtonInSuccessState( $this );
					disableAllButtonsExcept( $this );
					successMessage( response.message );
				}
				else {
					putButtonInErrorState( $this );
					errorMessage( response.message );
				}
			});
			
		} else {
			errorMessage( "Voting is currently disabled" );
			disableAllButtons();
			hideAllbuttons();
		}

        e.preventDefault();
    });

	
	
    var putButtonInLoadingState = function ( element ){
        element.attr('disabled' , 'disabled');
        element.removeClass('btn-success').removeClass('btn-danger').removeClass('btn-primary');
        element.addClass('btn-info');
        element.html("<i class=\"fa fa-circle-o-notch fa-spin\"></i>");
        return true;
    }

    var putButtonInSuccessState = function ( element ) {
        element.html("<i class=\"fa fa-check\"></i>");
        element.removeClass('btn-danger').removeClass('btn-info').removeClass('btn-primary');
        element.addClass('btn-success');
        return true;
    }

    var putButtonInErrorState = function ( element ) {
        element.html("<i class=\"fa fa-times\"></i>");
        element.removeClass('btn-success').removeClass('btn-info').removeClass('btn-primary');
        element.addClass('btn-danger');
        return true;
    }

    var disableAllButtonsExcept = function ( $this ) {
        var buttons = $('.vote').not( $this );
        buttons.attr( 'disabled' , 'disabled' );
    }


    var successMessage = function( message ) {
        $('#success_text').html( message );
        $('#success_message').slideDown().delay(1800).slideUp();
    }


    var errorMessage = function( message ) {
        $('#error_text').html( message );
        $('#error_message').slideDown().delay(1800).slideUp();
    }


    var disableAllButtons = function() {
        var buttons = $('.vote');
        buttons.attr( 'disabled' , 'disabled' );
    }

    var hideAllButtons = function() {
        var buttons = $('.vote');
        buttons.hide();
    }


    $(function(){
		if( ! $votingIsEnabled ) {
			disableAllButtons();
			hideAllButtons();
		}


    });
	
	
	
	
	var aTargetIsOpen = false;

	$('.winner-button').click(function( event ){

        var $this = $(this);
		var $thisButton = $this;
		var $thisWeek = $this.data('week');
        var $thisTarget = $('#week' + $thisWeek );
		var $thisVideo = $('#week' + $thisWeek + '_video');
		var $allTargets = $('.winners');
		var $allButtons = $('.winner-button');
		var $thisVideoController = videojs('week' + $thisWeek + '_video');
		var $videoWeek1 = videojs('week1_video');
		var $videoWeek2 = videojs('week2_video');
		var $videoWeek3 = videojs('week3_video');
		var $videoWeek4 = videojs('week4_video');
		var $videoWeek5 = videojs('week5_video');
		var $videoWeek6 = videojs('week6_video');
		var $videoWeek7 = videojs('week7_video');
		var $videoWeek8 = videojs('week8_video');
		var $videoWeek9 = videojs('week9_video');
		var $videoWeek10 = videojs('week10_video');
		var $videoWeek11 = videojs('week11_video');
		var $videoWeek12 = videojs('week12_video');
		var $videoWeek13 = videojs('week13_video');
		var $videoWeek14 = videojs('week14_video');
		
		
        if( $thisTarget.hasClass('open') ) {

			$thisVideo.fadeOut(function(){
				$thisTarget.removeClass('open').slideUp();
				$thisButton.removeClass('active');
			
			});
		
			aTargetIsOpen = false;

        } else {
		
			if( aTargetIsOpen ) {
			
				$allButtons.removeClass('active');
				$allTargets.hide().removeClass('open');
				$thisTarget.addClass('open').show();	
				$thisVideo.show();
			}
			else {
				$thisTarget.addClass('open').slideDown(function(){
					$thisVideo.fadeIn();
				});	
			}
			
			$thisButton.addClass('active');
			aTargetIsOpen = true;
			
		}
		
		$videoWeek1.pause();
		$videoWeek2.pause();
		$videoWeek3.pause();
		$videoWeek4.pause();
		$videoWeek5.pause();
		$videoWeek6.pause();
		$videoWeek7.pause();
		$videoWeek8.pause();
		$videoWeek9.pause();
		$videoWeek10.pause();
		$videoWeek11.pause();
		$videoWeek12.pause();
		$videoWeek13.pause();
		$videoWeek14.pause();
        event.preventDefault();

    });
	
	
	
	
	$('.team-header').click(function(e){
		
		var $this = $(this);
		var $arrow = $this.children('.team-header-inner').children('.team-arrow');
		var $teamContainer = $this.next('.team-players');
		
			
		if( $this.hasClass('open') ) {
			$arrow.removeClass('active');
			$teamContainer.slideUp(function(){
				$this.removeClass('open');
			});
		} else {
			$teamContainer.slideDown();
			$arrow.addClass('active');
			$this.addClass('open');
		}
	

		e.preventDefault();
	});
	


	$('.featured-player-play-button').click(function( event ){

		var $video = $(this).data('video');
		var $featuredVideoController = videojs('featured-video');
		var $featuredVideoContainer = $('#featured-video');
		var $featuredVideoSourceApi = $('#featured-video_html5_api');
		var $featuredVideoSource = $('#featured-video-source');

		$("img.vjs-poster").hide();
		$featuredVideoSource.attr('src',$video);
		$featuredVideoSourceApi.attr('src',$video);
		$featuredVideoController.ready( function(){
			$featuredVideoController.load();
			$('.fullscreen-player').fadeIn();
			$('.fullscreen-player-inner').fadeIn();
			$('.fullscreen-player-video').fadeIn();
			$('.fullscreen-player-close-button').fadeIn();
			$featuredVideoController.play();
		});

		event.preventDefault();
	});

	$('.fullscreen-player-close-button').click(function(event){

		var $featuredVideoController = videojs('featured-video');

		$featuredVideoController.ready( function(){
			$featuredVideoController.pause();
			$('.fullscreen-player').fadeOut();
			$('.fullscreen-player-inner').fadeOut();
			$('.fullscreen-player-video').fadeOut();
			$('.fullscreen-player-close-button').fadeOut();
		});



		event.preventDefault();
	});



});